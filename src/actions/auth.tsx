import auth from '@react-native-firebase/auth';
import { client } from '@graphql/config';
import { SET_TOKEN_MESSAGING } from '@graphql/mutation';

export const USER_LOGIN = 'USER_REQUEST_LOGIN';
export const USER_LOGOUT = 'USER_LOGOUT';

export const setTokenAction = (accessToken: string) => async (
  dispatch: Function,
) => {
  return dispatch({
    type: USER_LOGIN,
    data: { accessToken },
  });
};

export const setMessagingTokenAction = (
  accessToken: string,
  token: string,
) => async (dispatch: Function) => {
  const res = await client.mutate({
    variables: { input: { token } },
    mutation: SET_TOKEN_MESSAGING,
    context: { headers: { authorization: `Bearer ${accessToken}` } },
  });
};

export const logoutAction = () => async (dispatch: Function) => {
  auth().signOut();

  return dispatch({ type: USER_LOGOUT });
};
