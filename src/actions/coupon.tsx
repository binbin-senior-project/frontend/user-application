import { client } from '@graphql/config';
import { GET_COUPONS } from '@graphql/query';

export const GET_COUPONS_REQUEST = 'GET_USER_REQUEST';
export const GET_COUPONS_SUCCESS = 'GET_USER_SUCCESS';
export const GET_COUPONS_ERROR = 'GET_USER_ERROR';

export const getUserAction = () => async (dispatch: Function) => {
  dispatch({ type: GET_COUPONS_REQUEST });

  const { data } = await client.query({
    query: GET_COUPONS,
  });

  if (data.getCoupons) {
    const {
      id,
      name,
      description,
      condition,
      point,
      expire,
      storeID,
      categoryID,
    } = data.getCoupons;

    return dispatch({
      type: GET_COUPONS_SUCCESS,
      data: {
        id,
        name,
        description,
        condition,
        point,
        expire,
        storeID,
        categoryID,
      },
    });
  }

  return dispatch({
    type: GET_COUPONS_ERROR,
    data: { message: 'Something error' },
  });
};
