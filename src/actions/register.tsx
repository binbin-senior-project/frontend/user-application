import auth from '@react-native-firebase/auth';
import messaging from '@react-native-firebase/messaging';
import { client } from '@graphql/config';
import { REQUEST_CREATE_USER } from '@graphql/mutation';
import { setTokenAction, setMessagingTokenAction } from '@actions/auth';
import { setProfileAction } from './user';

export const REGISTER_RESET = 'REGISTER_RESET';
export const REGISTER_SET_PHONE = 'REGISTER_SET_PHONE';
export const REGISTER_SET_FIRSTNAME = 'REGISTER_SET_FIRSTNAME';
export const REGISTER_SET_LASTNAME = 'REGISTER_SET_LASTNAME';
export const REGISTER_SET_EMAIL = 'REGISTER_SET_EMAIL';
export const REGISTER_SET_PASSWORD = 'REGISTER_SET_PASSWORD';
export const REGISTER_SET_PASSCODE = 'REGISTER_SET_PASSCODE';
export const REGISTER_REQUEST = 'REGISTER_REQUEST';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_ERROR = 'REGISTER_ERROR';

export const setPhoneAction = (phone: string) => {
  return (dispatch: Function) => {
    dispatch({
      type: REGISTER_SET_PHONE,
      phone,
    });
  };
};

export const setFirstnameAction = (firstname: string) => {
  return (dispatch: Function) => {
    dispatch({
      type: REGISTER_SET_FIRSTNAME,
      firstname,
    });
  };
};

export const setLastnameAction = (lastname: string) => {
  return (dispatch: Function) => {
    dispatch({
      type: REGISTER_SET_LASTNAME,
      lastname,
    });
  };
};

export const setEmailAction = (email: string) => {
  return (dispatch: Function) => {
    dispatch({
      type: REGISTER_SET_EMAIL,
      email,
    });
  };
};

export const setPasswordAction = (password: string) => {
  return (dispatch: Function) => {
    dispatch({
      type: REGISTER_SET_PASSWORD,
      password,
    });
  };
};

export const setPasscodeAction = (passcode: string) => {
  return (dispatch: Function) => {
    dispatch({
      type: REGISTER_SET_PASSCODE,
      passcode,
    });
  };
};

export const registerAccountAction = () => async (
  dispatch: Function,
  getState: any,
) => {
  const {register} = getState();
  const {
    email, 
    password,
    firstname,
    lastname,
    phone,
    passcode,
  } = register;

  dispatch({ type: REGISTER_REQUEST });

  const response = await auth().createUserWithEmailAndPassword(email, password);

  if (response.user) {

    const accessToken = (await response.user?.getIdToken()).toString();
    const messagingToken = (await messaging().getToken()).toString();

    const userInput = {
      firstname,
      lastname,
      email,
      phone,
      photo: '',
      passcode,
    };

    const { data } = await client.mutate({
      variables: { input: userInput },
      mutation: REQUEST_CREATE_USER,
      context: { headers: { authorization: `Bearer ${accessToken}` } },
    });

    if (data?.createUser) {
      setMessagingTokenAction(accessToken, messagingToken)(dispatch);
      setTokenAction(accessToken)(dispatch);
      setProfileAction()(dispatch, getState);

      return dispatch({type: REGISTER_SUCCESS});
    }
  }

  return dispatch({type: REGISTER_ERROR, error: {message: 'Something wrong!'}});
};

export const resetRegisterAction = () => (dispatch: Function) => {
  dispatch({
    type: REGISTER_RESET,
  });
};
