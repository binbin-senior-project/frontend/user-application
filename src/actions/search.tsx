export const SET_COUPON_SEARCH = 'SET_COUPON_SEARCH';
export const SET_STORE_SEARCH = 'SET_STORE_SEARCH';
export const RESET_SEARCH = 'RESET_SEARCH';

export const setCouponSearchAction = (keyword: string) => (
  dispatch: Function,
) => {
  dispatch({
    type: SET_COUPON_SEARCH,
    keyword,
  });
};

export const setStoreSearchAction = (keyword: string) => (
  dispatch: Function,
) => {
  dispatch({
    type: SET_STORE_SEARCH,
    keyword,
  });
};

export const resetSearchAction = () => (dispatch: Function) => {
  dispatch({
    type: RESET_SEARCH,
  });
};
