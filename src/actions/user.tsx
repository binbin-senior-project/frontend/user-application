import { client } from '@graphql/config';
import { GET_USER, GET_USER_POINT } from '@graphql/query';

export const UPDATE_PROFILE = 'UPDATE_PROFILE';
export const UPDATE_LATEST_POST = 'UPDATE_LATEST_POST';

export const setProfileAction = () => async (dispatch: Function, getState: any) => {
  const { auth } = getState();
  const { accessToken } = auth;

  const { data } = await client.query({
    query: GET_USER,
    fetchPolicy: 'no-cache',
    context: { headers: { authorization: `Bearer ${accessToken}` } },
  });

  const { firstname, lastname, phone, photo, point } = data?.getUser;

  dispatch({
    type: UPDATE_PROFILE,
    data: {
      firstname,
      lastname,
      phone,
      photo,
      point,
    }
  })
};

export const updateLatestPointAction = () => async (
  dispatch: Function,
  getState: any,
) => {
  const { auth } = getState();
  const { accessToken } = auth;

  const { data } = await client.query({
    query: GET_USER_POINT,
    fetchPolicy: 'no-cache',
    context: { headers: { authorization: `Bearer ${accessToken}` } },
  });

  dispatch({
    type: UPDATE_LATEST_POST,
    point: data?.getUser?.point,
  })
};
