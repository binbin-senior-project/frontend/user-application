import React from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import PropTypes from 'prop-types';
import _ from 'lodash';
import Divider from '@components/Divider';
import fonts from '@utils/fonts';
import Coin from '@components/Coin';

const styles = StyleSheet.create({
  columnHeader: {
    ...fonts.h2Black,
  },
  smallHeader: {
    ...fonts.h3,
  },
  row: {
    flexDirection: 'row',
    paddingVertical: 5,
    paddingHorizontal: 5,
  },
  leftColumn: {
    width: Dimensions.get('screen').width / 3,
  },
  centerColumn: {
    width: Dimensions.get('screen').width / 3,
  },
  rightColumn: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  point: {
    textAlign: 'center',
    marginRight: 5,
  },
});

interface BottleTableProps {
  bottles: Array<{
    code: string;
    point: number;
  }>;
}

const BottleTable = ({ bottles }: BottleTableProps) => {
  // Group up similar type of bottle
  const renderBottles = _([...bottles])
    .groupBy('code')
    .map(items => {
      items[0].items = items.length;
      items[0].points = items.length * items[0].point;
      return items[0];
    })
    .value();

  // Calculate bottle points
  let points = 0;
  for (let i = 0; i < renderBottles.length; i++) {
    points += renderBottles[i].points;
  }

  return (
    <>
      <View>
        <View style={styles.row}>
          <View style={styles.leftColumn}>
            <Text style={styles.columnHeader}>ประเภท</Text>
          </View>
          <View style={styles.centerColumn}>
            <Text style={[styles.columnHeader, { textAlign: 'center' }]}>
              จำนวน
            </Text>
          </View>
          <View style={styles.rightColumn}>
            <Text style={[styles.columnHeader, { textAlign: 'right' }]}>
              แต้ม
            </Text>
          </View>
        </View>
        <Divider />

        {renderBottles.map((bottle: any) => (
          <>
            <View style={styles.row}>
              <View style={styles.leftColumn}>
                <Text style={styles.smallHeader}>{bottle.code}</Text>
              </View>
              <View style={styles.centerColumn}>
                <Text style={[styles.smallHeader, { textAlign: 'center' }]}>
                  x{bottle.items}
                </Text>
              </View>
              <View style={styles.rightColumn}>
                <Text style={[styles.smallHeader, styles.point]}>
                  {bottle.points}
                </Text>
                <Coin />
              </View>
            </View>
            <Divider />
          </>
        ))}

        <View style={styles.row}>
          <View style={styles.leftColumn}>
            <Text style={styles.smallHeader}>ยอดรวม:</Text>
          </View>
          <View style={styles.centerColumn}>
            <Text style={[styles.smallHeader, { textAlign: 'center' }]}>
              x{bottles.length}
            </Text>
          </View>
          <View style={styles.rightColumn}>
            <Text style={[styles.smallHeader, styles.point]}>{points}</Text>
            <Coin />
          </View>
        </View>
      </View>
    </>
  );
};

BottleTable.propTypes = {
  bottles: PropTypes.array.isRequired,
};

export default BottleTable;
