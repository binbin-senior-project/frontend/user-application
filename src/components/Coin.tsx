import React from 'react';
import { Row, Span } from './Global';

const Coin = () => (
  <Row
    alignCenter
    center
    style={{
      width: 20,
      height: 20,
      borderRadius: 15,
      backgroundColor: '#ffd52d',
    }}>
    <Row
      alignCenter
      center
      style={{
        width: 15,
        height: 15,
        borderRadius: 15,
        backgroundColor: '#d2a700',
      }}>
      <Span style={{
        fontSize: 14,
        color: '#f9cd00',
      }}>
        P
      </Span>
    </Row>
  </Row>
);

export default Coin;