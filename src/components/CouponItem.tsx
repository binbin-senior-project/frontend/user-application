import React from 'react';
import { Image, TouchableWithoutFeedback, View } from 'react-native';
import { withNavigation } from '@react-navigation/compat';
import Coin from '@components/Coin';
import { Row, Col, Span, P, H2 } from '@components/Global';
import colors from '@utils/colors';

interface Props {
  id: string;
  name: string;
  image: string;
  expire: string;
  point: string;
  store: any;
  navigation: any;
}

const CouponItem = ({
  id,
  name,
  image,
  point,
  expire,
  store,
  navigation,
}: Props) => {
  return (
    <>
      <TouchableWithoutFeedback
        onPress={() => {
          navigation.navigate('CouponDetailScreen', { couponID: id });
        }}>
        <View
          style={{
            paddingVertical: 15,
            paddingHorizontal: 20,
            borderRadius: 5,
            backgroundColor: colors.whiteColor,
            marginBottom: 10,
            borderRightWidth: 1,
            borderRightColor: colors.inputBackgroundColor,
            borderBottomWidth: 2,
            borderBottomColor: colors.inputBackgroundColor,
          }}>
          <Row alignCenter>
            <Col style={{ paddingRight: 20 }}>
              <Image
                source={{ uri: image }}
                style={{
                  width: 100,
                  height: 110,
                  borderRadius: 5,
                }}
              />
            </Col>
            <Col style={{ flex: 1 }}>
              <H2>{name}</H2>
              <Span style={{ marginBottom: 10 }}>{store.name}</Span>
              <Row between alignCenter>
                <Col>
                  <Span>{`ถึง ${expire}`}</Span>
                </Col>
                <Row alignCenter>
                  <P bold style={{ marginRight: 5 }}>
                    {point}
                  </P>
                  <Coin />
                </Row>
              </Row>
            </Col>
          </Row>
        </View>
      </TouchableWithoutFeedback>
    </>
  );
};

export default withNavigation(CouponItem);
