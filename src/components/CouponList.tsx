import React from 'react';
import {
  View,
  FlatList,
  ActivityIndicator,
  RefreshControl,
  Dimensions,
} from 'react-native';
import CouponItem from '@components/CouponItem';
import NoData from '@components/NoData';
import { Col } from '@components/Global';
import colors from '@utils/colors';

interface Props {
  isLoading: boolean;
  scrollEnabled?: boolean;
  coupons: any;
  onEndReached: Function;
  onRefresh: Function;
}

const CouponList = ({
  isLoading,
  scrollEnabled,
  coupons,
  onEndReached,
  onRefresh,
}: Props) => {
  return (
    <Col style={{ flex: 1, paddingHorizontal: 10 }}>
      {!isLoading && coupons.length == 0 && <NoData title="ไม่มีคูปอง" />}
      <FlatList
        scrollEnabled={scrollEnabled}
        data={coupons}
        refreshControl={
          <RefreshControl
            refreshing={isLoading}
            onRefresh={() => onRefresh()}
            tintColor={colors.primaryColor}
          />
        }
        onEndReached={() => onEndReached()}
        onEndReachedThreshold={0.8}
        numColumns={1}
        renderItem={({ item }: { item: any }) => {
          return (
            <CouponItem
              key={item.id}
              id={item.id}
              image={item.photo}
              name={item.name}
              point={item.point}
              expire={item.expire}
              store={item.store}
            />
          );
        }}
        keyExtractor={item => item.id}
        contentContainerStyle={{
          marginTop: 10,
          paddingBottom: 10,
        }}
      />
    </Col>
  );
};

export default CouponList;
