import React from 'react';
import {
  View,
  TouchableOpacity,
} from 'react-native';
import Divider from './Divider';
import colors from '@utils/colors';
import { H3, P } from '@components/Global';

interface DialogProps {
  title: string;
  description: string;
  textCancle: string;
  textConfirm: string;
  onCancle: Function;
  onConfirm: Function;
}

const Dialog = ({
  title,
  description,
  textCancle,
  textConfirm,
  onCancle,
  onConfirm
}: DialogProps) => (
  <View style={{
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 2,
  }}>
    <View style={{
      width: '100%',
      borderRadius: 3,
      paddingHorizontal: 20,
      paddingVertical: 15,
      shadowColor: 'rgba(0, 0, 0, 0.05)',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowRadius: 4,
      shadowOpacity: 1,
      backgroundColor: colors.whiteColor,
    }}>
      <H3 style={{ marginBottom: 10 }}>
        {title}
      </H3>
      <Divider isWhite />
      <P style={{
        marginTop: 10,
        marginBottom: 20,
      }}>
        {description}
      </P>
      <View style={{
        flexDirection: 'row',
        justifyContent: 'flex-end',
      }}>
        <TouchableOpacity
          onPress={() => onCancle}
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            minWidth: 80,
            paddingVertical: 5,
            paddingHorizontal: 20,
            borderRadius: 20,
            marginRight: 10,
            backgroundColor: colors.whiteBorder,
          }}>
          <P>{textCancle}</P>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => onConfirm()}
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            minWidth: 80,
            paddingVertical: 5,
            paddingHorizontal: 20,
            borderRadius: 20,
            backgroundColor: colors.primaryColor,
          }}>
          <P white>
            {textConfirm}
          </P>
        </TouchableOpacity>
      </View>
    </View>
  </View>
);

export default Dialog;
