import React from 'react';
import { Image } from 'react-native';
import GreenLogo from '@images/logo_green/logo.png';
import GreyLogo from '@images/logo_grey/logo.png';

interface Props {
  width?: number;
  height?: number;
  isGrey?: boolean;
}

const Logo = ({
  width = 180,
  height = 220,
  isGrey = false,
}: Props) => {
  const logo = isGrey ? GreyLogo : GreenLogo

  return (
    <Image
      style={{ width, height }}
      source={logo}
      resizeMode="contain"
    />
  );
}

export default Logo;
