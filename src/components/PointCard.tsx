import React from 'react';
import { View } from 'react-native';
import Logo from './Logo';
import { Col, Card, H2, H3 } from '@components/Global';
import colors from '@utils/colors';

interface Props {
  point: string;
}

const PointCard = ({ point }: Props) => (
  <View
    style={{
      backgroundColor: colors.whiteColor,
      marginBottom: 10,
      padding: 20,
    }}>
    <Col
      style={{
        marginTop: 10,
        marginLeft: 10,
      }}>
      <H3>คุณมีแต้มสะสมอยู่</H3>
      <H2 green style={{ fontSize: 100 }}>
        {point ? point : 0}
      </H2>
    </Col>
    <Col
      style={{
        position: 'absolute',
        right: 30,
        top: 20,
      }}>
      <Logo width={70} />
    </Col>
  </View>
);

export default PointCard;
