import React from 'react';
import {
  Image,
  TouchableWithoutFeedback,
  Dimensions,
  ImageSourcePropType,
} from 'react-native';
import { withNavigation } from '@react-navigation/compat';
import colors from '@utils/colors';
import { Col, P } from '@components/Global';

interface StoreBoxProps {
  id: string;
  name: string;
  logo: ImageSourcePropType;
  navigation: any;
}

const StoreBox = ({ id, name, logo, navigation }: StoreBoxProps) => {
  const onPress = () => {
    navigation.push('StoreDetailScreen', { storeID: id });
  };

  return (
    <TouchableWithoutFeedback onPress={() => onPress()}>
      <Col
        style={{
          width: Dimensions.get('screen').width / 2 - 15,
          borderRadius: 5,
          backgroundColor: colors.whiteColor,
          position: 'relative',
          overflow: 'hidden',
          marginBottom: 10,
          borderRightWidth: 1,
          borderRightColor: colors.inputBackgroundColor,
          borderBottomWidth: 2,
          borderBottomColor: colors.inputBackgroundColor,
        }}>
        <Col
          alignCenter
          center
          style={{
            padding: 20,
            paddingTop: 30,
            paddingBottom: 20,
          }}>
          <Image
            source={{ uri: logo }}
            style={{
              width: 80,
              height: 80,
              marginBottom: 10,
              borderRadius: 60,
            }}
          />
          <P center numberOfLines={2}>
            {name}
          </P>
        </Col>
      </Col>
    </TouchableWithoutFeedback>
  );
};

export default withNavigation(StoreBox);
