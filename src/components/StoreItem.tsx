import React from 'react';
import { View, Image, TouchableWithoutFeedback } from 'react-native';
import { withNavigation } from '@react-navigation/compat';
import { Row, P, Col, Span } from '@components/Global';

interface StoreRowProps {
  id: string;
  logo: string;
  name: string;
  tagline: string;
  navigation: any;
}

const StoreItem = ({ id, logo, name, tagline, navigation }: StoreRowProps) => {
  return (
    <TouchableWithoutFeedback
      onPress={() => {
        navigation.push('StoreDetailScreen', { storeID: id });
      }}>
      <View style={{ padding: 20 }}>
        <Row alignCenter>
          <Col style={{ paddingRight: 20 }}>
            <Image
              source={{ uri: logo }}
              style={{
                width: 60,
                height: 60,
                borderRadius: 5,
              }}
            />
          </Col>
          <Col
            style={{
              flex: 1,
            }}>
            <P numberOfLines={2}>{name}</P>
            <Span style={{ marginBottom: 5 }}>{tagline}</Span>
          </Col>
        </Row>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default withNavigation(StoreItem);
