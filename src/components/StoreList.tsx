import React from 'react';
import {
  View,
  FlatList,
  ActivityIndicator,
  RefreshControl,
} from 'react-native';
import StoreItem from '@components/StoreItem';
import NoData from '@components/NoData';
import colors from '@utils/colors';
import { Col } from './Global';
import StoreBox from '@components/StoreBox';

interface Props {
  isLoading: boolean;
  scrollEnabled: boolean;
  stores: any;
  onEndReached: Function;
  onRefresh: Function;
}

const StoreList = ({
  isLoading,
  scrollEnabled,
  stores,
  onEndReached,
  onRefresh,
}: Props) => {
  return (
    <Col style={{ flex: 1 }}>
      {!isLoading && stores.length == 0 && <NoData title="ไม่มีคูปอง" />}
      <FlatList
        scrollEnabled={scrollEnabled}
        data={stores}
        refreshControl={
          <RefreshControl
            refreshing={isLoading}
            onRefresh={() => onRefresh()}
            tintColor={colors.primaryColor}
          />
        }
        onEndReached={() => onEndReached()}
        onEndReachedThreshold={0.8}
        numColumns={2}
        columnWrapperStyle={{
          justifyContent: 'space-between',
        }}
        renderItem={({ item }: { item: any }) => {
          return (
            <>
              <StoreBox
                id={item.id}
                name={item.name}
                logo={item.logo}
                tagline={item.tagline}
              />
            </>
          );
        }}
        keyExtractor={item => item.id}
        contentContainerStyle={{
          padding: 10,
        }}
      />
    </Col>
  );
};

export default StoreList;
