import React, { useState, useEffect } from 'react';
import {
  Animated,
  Dimensions,
  TouchableWithoutFeedback,
  ScrollView,
  Easing,
  TouchableOpacity,
} from 'react-native';
import { withNavigation } from '@react-navigation/compat';
import { Row, H3, Col } from '@components/Global';
import colors from '@utils/colors';
import Icon from 'react-native-vector-icons/Feather';
import LinearGradient from 'react-native-linear-gradient';

interface Props {
  items: Array<Object>;
  onPress: Function;
  onPressSearch: Function;
  navigation: any;
}

const Tab = ({ items, onPress, onPressSearch, navigation }: Props) => {
  const [borderPosX] = useState(new Animated.Value(0));

  const screenWidth = Dimensions.get('window').width;
  const borderWidth = 90;
  const duration = 0;

  const moveBorderHeader = (index: any, id: string) => {
    const toValue = index * borderWidth;

    const options = {
      toValue,
      duration,
      easing: Easing.linear,
      useNativeDriver: true,
    };

    onPress(id);

    Animated.timing(borderPosX, options).start();
  };

  useEffect(() => {
    const unsubscribe = navigation.addListener('blur', () => {
      const options = {
        toValue: 0,
        duration: 0,
        useNativeDriver: true,
      };

      Animated.timing(borderPosX, options).start();
    });

    return unsubscribe;
  }, [navigation]);

  return (
    <Row
      alignCenter
      style={{
        backgroundColor: colors.whiteColor,
        borderBottomWidth: 1,
        borderBottomColor: colors.inputBackgroundColor,
      }}>
      <ScrollView horizontal>
        <Col>
          <Row alignCenter>
            {items.map((item: any, index: Number) => (
              <TouchableWithoutFeedback
                key={item.title}
                onPress={() => moveBorderHeader(index, item.id)}>
                <Col
                  style={{
                    width: 90,
                    paddingTop: 15,
                    paddingBottom: 12,
                  }}>
                  <H3 center>{item.title}</H3>
                </Col>
              </TouchableWithoutFeedback>
            ))}
          </Row>
          <Animated.View
            style={{
              width: 90,
              backgroundColor: colors.primaryColor,
              height: 3,
              transform: [
                {
                  translateX: borderPosX.interpolate({
                    inputRange: [0, screenWidth],
                    outputRange: [0, screenWidth],
                  }),
                },
              ],
            }}
          />
        </Col>
      </ScrollView>
      <Row style={{ position: 'relative', paddingHorizontal: 20 }}>
        <LinearGradient
          start={{ x: 0, y: 1 }}
          end={{ x: 1, y: 1 }}
          colors={[
            'rgba(255, 255, 255, 0.1)',
            'rgba(255, 255, 255, 0.5)',
            'rgba(255, 255, 255, 0.9)',
            'rgba(255, 255, 255, 1)',
          ]}
          style={{
            position: 'absolute',
            left: -30,
            top: -5,
            width: 30,
            height: 30,
            zIndex: 2,
          }}
        />
        <TouchableOpacity onPress={() => onPressSearch()}>
          <Icon name={'search'} size={25} color={colors.fontColor} />
        </TouchableOpacity>
      </Row>
    </Row>
  );
};

export default withNavigation(Tab);
