import React from 'react';
import { View, TouchableWithoutFeedback } from 'react-native';
import moment from 'moment';
import { Row, Col, P, Span } from '@components/Global';
import Coin from '@components/Coin';
import colors from '@utils/colors';

interface Props {
  id: string;
  point: number;
  description: string;
  createdAt: string;
  onPress: Function;
}

const TransactionListItem = ({
  id,
  point,
  description,
  createdAt,
  onPress,
}: Props) => {
  const d = Date.parse(createdAt.replace(/-/gi, '/').split('.')[0]);
  const format = moment(d).locale('th');
  createdAt = format.add(543, 'y').format('ll HH:mm');

  return (
    <TouchableWithoutFeedback onPress={() => onPress(id)}>
      <View
        style={{
          padding: 20,
          borderBottomWidth: 1,
          borderBottomColor: colors.grayBorder,
          backgroundColor: colors.whiteColor,
        }}>
        <Row between>
          <Col>
            <P>{description}</P>
            <Span>{createdAt}</Span>
          </Col>
          <Row alignCenter>
            <P
              style={{
                color: point > 0 ? colors.primaryColor : colors.redColor,
                marginRight: 5,
              }}>
              {point}
            </P>
            <Coin />
          </Row>
        </Row>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default TransactionListItem;
