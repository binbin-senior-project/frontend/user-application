import React from 'react';
import { StatusBar } from 'react-native';
import PointButton from '@components/buttons/PointButton';
import colors from '@utils/colors';
import AvatarButton from '@components/buttons/AvatarButton';
import { Row, H1 } from '@components/Global';

interface Props {
  title: string;
  avatarImage?: string;
  onPressAvatar?: Function;
  onPressPoint: Function;
  point: string;
}

const Appbar = ({
  title,
  avatarName,
  avatarImage,
  onPressAvatar,
  onPressPoint,
  point,
}: Props) => {
  return (
    <Row
      between
      alignCenter
      style={{
        width: '100%',
        paddingHorizontal: 20,
        paddingTop: 35,
        backgroundColor: colors.primaryColor,
      }}>
      <StatusBar barStyle="light-content" />
      <Row alignCenter>
        <AvatarButton avatar={avatarImage} onPress={() => onPressAvatar()} />
        <H1 white style={{ margin: 10 }}>
          {title}
        </H1>
      </Row>
      <PointButton point={point} onPress={() => onPressPoint()} />
    </Row>
  );
};

export default Appbar;
