import React from 'react';
import { StatusBar, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import colors from '../../utils/colors';
import { Row, H1 } from '@components/Global';
import AvatarButton from '@components/buttons/AvatarButton';
import PointButton from '@components/buttons/PointButton';

interface Props {
  title: string;
  avatarImage?: string;
  point: string;
  onPressAvatar: Function;
}

const AppbarSearch = ({ title, avatarImage, point, onPressAvatar }: Props) => (
  <Row
    between
    alignCenter
    style={{
      width: '100%',
      paddingHorizontal: 20,
      paddingTop: 45,
      paddingBottom: 10,
      backgroundColor: colors.primaryColor,
    }}>
    <StatusBar barStyle="light-content" />
    <Row alignCenter>
      <AvatarButton avatar={avatarImage} onPress={() => onPressAvatar()} />
      <H1 white style={{ marginLeft: 10 }}>
        {title}
      </H1>
    </Row>
    <PointButton point={point} onPress={() => onPressPoint()} />
  </Row>
);

export default AppbarSearch;
