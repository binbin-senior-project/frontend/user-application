import React from 'react';
import { TouchableWithoutFeedback } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import colors from '@utils/colors';
import { H2, Row } from '@components/Global';

interface ButtonProps {
  text: string;
  showNextIcon?: boolean;
  isWhite?: boolean;
  onPress: Function;
}

const Button = ({ text, showNextIcon, isWhite, onPress }: ButtonProps) => (
  <TouchableWithoutFeedback onPress={() => onPress()}>
    <Row
      center
      style={{
        paddingVertical: 15,
        borderRadius: 50,
        backgroundColor: colors.primaryColor,
        marginTop: 10,
        marginBottom: 10,
        color: isWhite ? colors.whiteColor : colors.primaryColor,
      }}>
      <H2 white>{text}</H2>
      {showNextIcon && (
        <Icon
          name="arrow-right"
          size={30}
          color={colors.whiteColor}
          style={{ marginLeft: 10 }}
        />
      )}
    </Row>
  </TouchableWithoutFeedback>
);

export default Button;
