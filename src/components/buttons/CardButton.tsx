import React from 'react';
import { TouchableWithoutFeedback, View } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import colors from '@utils/colors';
import { H3, Card } from '@components/Global';

interface Props {
  text: string,
  onPress: Function,
}

const CardButton = ({ text, onPress }: Props) => (
  <TouchableWithoutFeedback onPress={onPress}>
    <Card>
      <H3>{text}</H3>
      <Icon
        name="chevron-right"
        size={25}
        color={colors.fontColor}
      />
    </Card>
  </TouchableWithoutFeedback>
);

export default CardButton;
