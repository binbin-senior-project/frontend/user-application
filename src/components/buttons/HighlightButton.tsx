import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import fonts from '@utils/fonts';

interface Props {
  text: string;
  onPress: Function;
}

const HighlightButton = ({ text, onPress }: Props) => (
  <TouchableOpacity
    style={{}}
    onPress={onPress}
  >
    <Text style={fonts.paragraphGreen}>
      {text}
    </Text>
  </TouchableOpacity>
);

export default HighlightButton;
