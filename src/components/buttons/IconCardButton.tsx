import React from 'react';
import { TouchableWithoutFeedback } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import { Row, Card, H3 } from '@components/Global';
import colors from '@utils/colors';

interface Props {
  icon: string;
  text: string;
  showChevron?: boolean;
  onPress: Function;
}

const IconCardButton = ({
  icon,
  text,
  showChevron,
  onPress,
}: Props) => (
  <TouchableWithoutFeedback onPress={() => onPress()}>
    <Card>
      <Row alignCenter>
        <Icon
          name={icon}
          size={18}
          color={colors.fontColor}
          style={{ marginRight: 15 }}
        />
        <H3>{text}</H3>
      </Row>
      {showChevron && (
        <Icon
          name="chevron-right"
          size={18}
          color={colors.fontColor}
        />
      )}
    </Card>
  </TouchableWithoutFeedback>
);

export default IconCardButton;
