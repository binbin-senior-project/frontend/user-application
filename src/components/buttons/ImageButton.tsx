import React from 'react';
import {
  TouchableOpacity,
  Image,
} from 'react-native';
import colors from '@utils/colors';
import { Row, H3 } from '@components/Global';

interface Props {
  ImageIcon: string;
  ImageName: string;
  onPress: Function;
}

const ImageButton = ({
  ImageIcon,
  ImageName,
  onPress,
}: Props) => (
  <TouchableOpacity
    style={{
      alignItems: 'center',
      padding: 15,
      width: '100%',
      backgroundColor: colors.whiteColor,
      flexDirection: 'row',
      justifyContent: 'space-between',
    }}
    onPress={() => onPress()}
  >
    <Row alignCenter>
      <Image
        source={ImageIcon}
        style={{
          width: 40,
          height: 40,
          marginLeft: 18,
        }}
      />
      <H3>{`แลกเข้าบัญชีธนาคาร ${ImageName}`}</H3>
    </Row>
  </TouchableOpacity>
);

export default ImageButton;
