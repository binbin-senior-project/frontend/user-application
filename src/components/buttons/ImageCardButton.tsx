import React from 'react';
import {
  TouchableWithoutFeedback,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import colors from '@utils/colors';
import {
  Row,
  H3,
  Card,
} from '@components/Global';

interface Props {
  text: string,
  image: string,
  onPress: Function,
}

const ImageCardButton = ({
  text,
  image,
  onPress,
}: Props) => (
  <TouchableWithoutFeedback onPress={() => onPress()}>
    <Card>
      <Row between>
        <Image
          source={image}
          style={{ marginRight: 20 }}
        />
        <H3>{text}</H3>
      </Row>
      <Icon
        name="chevron-right"
        size={25}
        color={colors.fontColor}
      />
    </Card>
  </TouchableWithoutFeedback>
);

export default ImageCardButton;
