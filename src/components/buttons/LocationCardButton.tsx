import React from 'react';
import { TouchableWithoutFeedback } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import colors from '@utils/colors';
import { Card, H3, Span, Row } from '@components/Global';

interface Props {
  place: string;
  address: string;
  onPress: Function;
}

const LocationCardButton = ({
  place,
  address,
  onPress,
}: Props) => (
  <TouchableWithoutFeedback onPress={() => onPress()}>
    <Card>
      <Icon
        name="map-pin"
        size={18}
        color={colors.fontColor}
        style={{ marginRight: 15 }}
      />
      <Row>
        <H3>{place}</H3>
        <Span>{address}</Span>
      </Row>
    </Card>
  </TouchableWithoutFeedback>
);

export default LocationCardButton;
