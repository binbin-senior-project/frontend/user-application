import React from 'react';
import { View, TouchableWithoutFeedback } from 'react-native';
import fonts from '@utils/fonts';
import colors from '@utils/colors';
import Coin from '@components/Coin';
import { P } from '@components/Global';

interface PointButton {
  point: string;
  onPress: Function;
}

const PointButton = ({ point, onPress }: PointButton) => (
  <TouchableWithoutFeedback onPress={() => onPress()}>
    <View style={{
      paddingVertical: 3,
      paddingHorizontal: 15,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 18,
      backgroundColor: colors.secondaryColor,
      flexDirection: 'row',
    }}>
      <P white style={{ marginRight: 10 }}>
        {`คุณมีแต้มอยู่: ${point}`}
      </P>
      <Coin />
    </View>
  </TouchableWithoutFeedback>
);

export default PointButton;
