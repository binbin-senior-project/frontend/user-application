import React from 'react';
import { TouchableWithoutFeedback } from 'react-native';
import colors from '@utils/colors';
import Coin from '@components/Coin';
import { Row, H3 } from '@components/Global';

interface RedeemButtonProps {
  point: number;
  onPress: Function;
}

const RedeemButton = ({ point, onPress }: RedeemButtonProps) => (
  <TouchableWithoutFeedback onPress={() => onPress()}>
    <Row
      alignCenter
      center
      style={{
        paddingVertical: 12,
        paddingHorizontal: 35,
        borderRadius: 30,
        backgroundColor: colors.primaryColor,
        flex: 1,
      }}>
      <H3 white>{`แลกคูปอง ${point}`}</H3>
      <Row style={{ marginLeft: 10 }}>
        <Coin />
      </Row>
    </Row>
  </TouchableWithoutFeedback>
);

export default RedeemButton;
