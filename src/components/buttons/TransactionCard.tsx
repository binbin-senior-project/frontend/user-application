import React from 'react';
import {
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import colors from '@utils/colors';
import {
  Row,
  H3,
  P,
  Span,
  Card,
} from '@components/Global';

interface Props {
  text: string;
  time: string;
  point: string;
  onPress: Function;
}

const TransactionCard = ({
  text,
  time,
  point,
  onPress,
}: Props) => {
  const isNegative = parseFloat(point) > 0;

  return (
    <TouchableWithoutFeedback onPress={() => onPress()}>
      <Card>
        <View>
          <P>{text}</P>
          <Span>{`เวลา ${time}`}</Span>
        </View>
        <Row alignCenter>
          <H3 style={{ color: isNegative ? colors.primaryColor : 'red' }}>
            {isNegative ? `+ ${point}` : point}
          </H3>
          <Icon
            name="chevron-right"
            size={25}
            color={colors.fontColor}
            style={{ marginLeft: 10 }}
          />
        </Row>
      </Card>
    </TouchableWithoutFeedback>
  );
}

export default TransactionCard;
