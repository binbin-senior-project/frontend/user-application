import React from 'react';
import { TextInput } from 'react-native';
import fonts from '@utils/fonts';
import colors from '@utils/colors';

interface Props {
  innerRef: any,
  placeholder: string;
}

const Textarea = ({ placeholder, innerRef }: Props) => (
  <TextInput
    ref={innerRef}
    style={[{
      width: '100%',
      height: 150,
      padding: 20,
      paddingTop: 20,
      borderRadius: 5,
      backgroundColor: colors.inputBackgroundColor,
    }, fonts.paragraph]}
    placeholder={placeholder}
    placeholderTextColor={fonts.placeholder.color}
    multiline
  />
);

export default React.forwardRef((props, ref) => <Textarea innerRef={ref} {...props} />);
