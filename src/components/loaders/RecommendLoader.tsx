import React from 'react';
import { View, ScrollView, Dimensions } from 'react-native';
import colors from '@utils/colors';
import ContentLoader, { Rect } from 'react-content-loader/native'

const RecommendLoader = () => {
  const width = Dimensions.get('window').width / 2 - 5;
  const height = 270;

  const contentWidth = width - 20;
  const contentHeight = height - 20

  return (
    <ScrollView
      horizontal
      contentContainerStyle={{ paddingVertical: 10, paddingHorizontal: 20 }}
      showsHorizontalScrollIndicator={false}
    >
      {[1, 2, 3].map((item: Number, index: Number) => (
        <View style={{
          width: Dimensions.get('screen').width / 2 - 25,
          height: 220,
          marginRight: index == 2 ? 0 : 10,
          borderRadius: 5,
          shadowColor: "rgba(0, 0, 0, 0.05)",
          shadowOffset: {
            width: 0,
            height: 2
          },
          shadowRadius: 4,
          shadowOpacity: 1,
          backgroundColor: colors.whiteColor,
          position: 'relative',
          overflow: 'hidden',
          marginBottom: 10,
          borderRightWidth: 1,
          borderRightColor: colors.inputBackgroundColor,
          borderBottomWidth: 2,
          borderBottomColor: colors.inputBackgroundColor,
        }}>
          <ContentLoader 
            speed={2}
            width={contentWidth}
            height={contentHeight}
            viewBox={`0 23 ${width} 250`}
            backgroundColor="#e9e9e9"
            foregroundColor="#eaeaea"
            style={{ marginRight: index == 2 ? 0 : 10, }}
          >
            <Rect x="0" y="0" rx="0" ry="15" width={width} height="140" />
            <Rect x="15" y="150" rx="0" ry="15" width={width - 30} height="20" />
            <Rect x="15" y="180" rx="0" ry="15" width={width - 30} height="20" />
            <Rect x="15" y="225" rx="0" ry="15" width={width - 30} height="15" />
          </ContentLoader>
        </View>
      ))}
    </ScrollView>
  );
}

export default RecommendLoader;