import React, { ReactNode } from 'react';
import { View, TouchableOpacity } from 'react-native';
import Divider from '@components/Divider';
import { H3, P } from '@components/Global';

interface PanelTransparentProps {
  title: string;
  description?: string;
  headerStyle?: Object;
  children: ReactNode;
  buttonText?: string;
  onPress?: Function;
}

const PanelTransparent = ({
  title,
  description,
  buttonText,
  headerStyle,
  onPress,
  children
}: PanelTransparentProps) => (
  <View style={{
    width: '100%',
    marginBottom: 10,
    flex: 1
  }}>
    <View style={headerStyle}>
      <View style={{
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
      }}>
        <H3 bold>{title}</H3>
        {!!onPress && (
          <TouchableOpacity onPress={() => onPress()}>
            <P green>
              {buttonText ? buttonText : 'ดูทั้งหมด'}
            </P>
          </TouchableOpacity>
          )
        }
      </View>
      <Divider />
    </View>
    {!!children ? (
      children
    ) : (
      <View style={{ padding: 20 }}>
        <P>{description}</P>
      </View>
    )}
  </View>
);

export default PanelTransparent;
