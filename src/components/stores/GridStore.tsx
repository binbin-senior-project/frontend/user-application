import React, { useState } from 'react';
import { FlatList, RefreshControl } from 'react-native';
import StoreBox from '@components/StoreBox';
import colors from '@utils/colors';

interface GridStoreProps {
  dataSource: any;
  scrollEnabled?: boolean;
}

const GridStore = ({ dataSource, scrollEnabled = true }: GridStoreProps) => {
  const [refresh, setRefesh] = useState(false);

  const onRefresh = () => {
    setRefesh(true);

    // Load dataSource
    setTimeout(() => {
      setRefesh(false);
    }, 1000);
  };

  return (
    <FlatList
      scrollEnabled={scrollEnabled}
      data={dataSource}
      numColumns={2}
      refreshControl={
        <RefreshControl
          refreshing={refresh}
          onRefresh={onRefresh}
          tintColor={colors.iconColor}
        />
      }
      renderItem={({ item }: { item: Object }) => {
        return (
          <StoreBox
            name={item.name}
            logo={item.logo}
            amount={item.amount}
            distance={item.distance}
            isFavorite={item.isFavorite}
          />
        );
      }}
      keyExtractor={item => item.id}
      contentContainerStyle={{ padding: 20 }}
      columnWrapperStyle={{ justifyContent: 'space-between' }}
    />
  );
};

export default GridStore;
