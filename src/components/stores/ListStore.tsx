import React, { useState } from 'react';
import { FlatList, RefreshControl } from 'react-native';
import StoreRow from '@components/StoreItem';
import colors from '@utils/colors';

interface ListStoreProps {
  dataSource: any;
  scrollEnabled?: boolean;
}

const ListStore = ({ dataSource, scrollEnabled = true }: ListStoreProps) => {
  const [refresh, setRefesh] = useState(false);

  const onRefresh = () => {
    setRefesh(true);

    // Load dataSource
    setTimeout(() => {
      setRefesh(false);
    }, 1000);
  };

  return (
    <FlatList
      scrollEnabled={scrollEnabled}
      data={dataSource}
      numColumns={1}
      refreshControl={
        <RefreshControl
          refreshing={refresh}
          onRefresh={onRefresh}
          tintColor={colors.iconColor}
        />
      }
      renderItem={({ item }: { item: Object }) => {
        return (
          <StoreRow
            name={item.name}
            logo={item.logo}
            description={item.description}
            amount={item.amount}
            distance={item.distance}
          />
        );
      }}
      keyExtractor={item => item.id}
      contentContainerStyle={{ padding: 20 }}
    />
  );
};

export default ListStore;
