import React, { useState, useRef } from 'react';
import {
  Animated,
  Dimensions,
  TouchableWithoutFeedback,
  ScrollView,
  Easing,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import GridStore from '@components/stores/GridStore';
import ListStore from '@components/stores/ListStore';
import colors from '@utils/colors';
import Icon from '@utils/icons';
import {
  Row,
  H3,
  Column,
} from '@components/Global';

interface Props {
  tabs: any;
}

const TabCoupon = ({ tabs }: Props) => {
  const [layout, setLayout] = useState('grid');
  const [borderPosX] = useState(new Animated.Value(0))
  const scrollViewRef = useRef(null);

  const screenWidth = Dimensions.get('window').width;
  const borderWidth = 90;
  const duration = 120;

  const onPageChange = (index: any) => {
    moveBorderHeader(index);
    moveScrollPage(index);
  };

  const moveBorderHeader = (index: any) => {
    const toValue = index * borderWidth;

    const options = {
      toValue,
      duration,
      easing: Easing.linear,
      useNativeDriver: true,
    };

    Animated.timing(borderPosX, options).start()
  }

  const moveScrollPage = (index: any) => {
    const { current } = scrollViewRef;
    const node = current?.getNode();
    const x = index * screenWidth;

    const options = {
      x: x,
      y: 0,
      animated: true,
    };

    node?.scrollTo(options);
  }

  const onScroll = Animated.event([{
    nativeEvent: {
      contentOffset: {
        x: borderPosX
      }
    }}],
    { useNativeDriver: true }
  )

  return (
    <>
      <Row alignCenter style={{
        backgroundColor: colors.whiteColor,
        borderBottomWidth: 1,
        borderBottomColor: colors.inputBackgroundColor,
      }}>
        <ScrollView horizontal>
          <Column>
            <Row alignCenter>
              {tabs.map((route: any, index: Number) => (
                <TouchableWithoutFeedback
                  key={route.title}
                  onPress={() => onPageChange(index)}
                >
                  <Column style={{
                    width: 90,
                    paddingTop: 15,
                    paddingBottom: 12
                  }}>
                    <H3 center>{route.title}</H3>
                  </Column>
                </TouchableWithoutFeedback>
              ))}
            </Row>
            <Animated.View
              style={{
                width: 90,
                backgroundColor: colors.primaryColor,
                height: 3,
                transform: [{
                  translateX: borderPosX.interpolate({
                    inputRange: [0, screenWidth],
                    outputRange: [0, screenWidth / 4 - 14]
                  })
                }],
              }}
            />
          </Column>
        </ScrollView>
        <Row style={{
          position: 'relative',
          paddingRight: 20,
        }}>
          <LinearGradient
            start={{ x: 0, y: 1 }}
            end={{ x: 1, y: 1 }}
            colors={[
              'rgba(255, 255, 255, 0.1)',
              'rgba(255, 255, 255, 0.5)',
              'rgba(255, 255, 255, 0.9)',
              'rgba(255, 255, 255, 1)'
            ]}
            style={{
              position: 'absolute',
              left: -30,
              top: -5,
              width: 30,
              height: 30,
              zIndex: 2,
          }} />
          <TouchableWithoutFeedback onPress={() => setLayout('grid')}>
            <Icon
              name="grid"
              size={20}
              style={[
                {
                  paddingLeft: 15,
                  color: colors.iconColor,
                },
                layout == 'grid' && { color: colors.primaryColor }
              ]}
            />
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={() => setLayout('list')}>
            <Icon
              name="list"
              size={20}
              style={[
                {
                  paddingLeft: 15,
                  color: colors.iconColor,
                },
                layout == 'list' && { color: colors.primaryColor }
              ]}
            />
          </TouchableWithoutFeedback>
        </Row>
      </Row>
      <Animated.ScrollView 
        style={{ width: '100%' }}
        horizontal
        pagingEnabled
        refreshControl
        scrollEventThrottle={16}
        onScroll={onScroll}
        showsHorizontalScrollIndicator={false}
        ref={scrollViewRef}
      >
        {tabs.map((tab: Object, index: Number) => (
          <Column key={index} style={{ width: screenWidth }}>
            {layout == 'grid' && (
              <GridStore
                dataSource={tab.dataSource}
              />
            )}
            
            {layout == 'list' && (
              <ListStore
                dataSource={tab.dataSource}
              />
            )}
          </Column>
        ))}
      </Animated.ScrollView>
    </>
  );
};

export default TabCoupon;
