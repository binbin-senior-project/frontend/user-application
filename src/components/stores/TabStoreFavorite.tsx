import React, { useState, useRef } from 'react';
import {
  View,
  Animated,
  Dimensions,
  TouchableWithoutFeedback,
  Easing,
} from 'react-native';
import MapView from 'react-native-maps';
import Icon from 'react-native-vector-icons/Feather';
import TabCoupon from '@components/coupons/TabCoupon';
import Panel from '@components/panels/Panel';
import colors from '@utils/colors';
import {
  H3,
  Row,
  Column,
} from '@components/Global';
import StoreCoupons from '@containers/stores/StoreCoupons';

interface TabStoreProps {
  
}

const TabStoreFavorite = ({
  description,
  location,
  coupons,
}: TabStoreProps) => {
  const [borderPosX] = useState(new Animated.Value(0))
  const scrollViewRef = useRef(null);
  const screenWidth = Dimensions.get('window').width;
  const borderWidth = 130;
  const duration = 120;

  const [scrollHeight, setScrollHeight] = useState(400);
  const [aboutHeight, setAboutHeight] = useState(400);

  const onPageChange = (index: any) => {
    moveBorderHeader(index);
    moveScrollPage(index);
  };

  const moveBorderHeader = (index: any) => {
    const toValue = index * borderWidth;

    const options = {
      toValue,
      duration,
      easing: Easing.linear,
      useNativeDriver: true,
    };

    Animated.timing(borderPosX, options).start()
  }

  const moveScrollPage = (index: any) => {
    const { current } = scrollViewRef;
    const node = current?.getNode();
    const x = index * screenWidth;

    const options = {
      x: x,
      y: 0,
      animated: true,
    };

    node?.scrollTo(options);
  }

  const onScroll = Animated.event([{
    nativeEvent: {
      contentOffset: {
        x: borderPosX
      }
    }}],
    { useNativeDriver: true }
  )

  const adjustScrollHeight = (event: any) => {
    const { nativeEvent } = event;
    const { contentOffset } = nativeEvent;
    const { width } = nativeEvent.layoutMeasurement;
    const index = Math.floor(contentOffset.x / width);

    if (index == 0) {
      return setScrollHeight(aboutHeight);
    }
    
    return setScrollHeight('auto');
  }

  return (
    <>
      <Row
        alignCenter
        between
        style={{
          backgroundColor: colors.whiteColor,
          borderBottomWidth: 1,
          borderBottomColor: colors.inputBackgroundColor,
        }}
      >
        <Column>
          <Row alignCenter>
            {['รายละเอียด', 'คูปองร้าน'].map((item: string, index: Number) => (
              <TouchableWithoutFeedback onPress={() => onPageChange(index)}>
                <Column style={{
                  width: 130,
                  paddingTop: 15,
                  paddingBottom: 12,
                }}>
                  <H3 center>{item}</H3>
                </Column>
              </TouchableWithoutFeedback>
            ))}
          </Row>
          <Animated.View
            style={{
              width: borderWidth,
              backgroundColor: colors.primaryColor,
              height: 3,
              transform: [{
                translateX: borderPosX.interpolate({
                  inputRange: [0, screenWidth],
                  outputRange: [0, screenWidth / 3 - 10]
                })
              }],
            }}
          />
        </Column>
        <Row style={{ paddingRight: 20 }}>
          <TouchableWithoutFeedback>
            <Row alignCenter>
              <Icon name="heart" size={20} />
              <H3 style={{ marginLeft: 7 }}>ถูกใจ</H3>
            </Row>
          </TouchableWithoutFeedback>
        </Row>
      </Row>
      <Animated.ScrollView 
        style={{ width: '100%', height: scrollHeight }}
        horizontal
        pagingEnabled
        scrollEventThrottle={16}
        onScroll={onScroll}
        showsHorizontalScrollIndicator={false}
        onMomentumScrollEnd={adjustScrollHeight}
        ref={scrollViewRef}
      >
        <View style={{
          width: screenWidth,
          marginTop: 10,
        }}>
          <View onLayout={(event) => {
            const { height } = event.nativeEvent.layout;
            setAboutHeight(height);
            setScrollHeight(height);
          }}>
            <Panel
              title="เกี่ยวกับ"
              description="เปิดทำการทุกวัน จันทร์ ถึง ศุกร์"
            />
            <Panel title="แผนที่">
              <MapView
                initialRegion={{
                  latitude: 37.78825,
                  longitude: -122.4324,
                  latitudeDelta: 0.0922,
                  longitudeDelta: 0.0421,
                }}
                style={{
                  width: '100%',
                  height: 250,
                }}
              />
            </Panel>
          </View>
        </View>
        <View style={{ width: screenWidth }}>
          <StoreCoupons />
          {/* <TabCoupon
            scrollEnabled={false}
            tabs={[{
              title: "ทั้งหมด",
              dataSource: coupons
            }, {
              title: "อาหาร",
              dataSource: coupons
            }, {
              title: "เครื่องดื่ม",
              dataSource: coupons
            }]}
          /> */}
        </View>
      </Animated.ScrollView>
    </>
  );
};

export default TabStoreFavorite;
