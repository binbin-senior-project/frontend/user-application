import React from 'react'
import { View, Image } from 'react-native'

import Swiper from 'react-native-swiper'
import colors from '@utils/colors'

interface ActivitySwiperProps {
  activities: Object;
};

const ActivitySwiper = ({
  activities,
}: ActivitySwiperProps) => {
  return (
    <Swiper
      style={{ height: 220 }}
      dotColor={colors.iconColor}
      activeDotColor={colors.primaryColor}
      showsPagination={false}
      loop={false}
      bounces
    >
      {activities.map((activity: Object) => (
        <View key={activity.id} style={{ padding: 20 }}>
          <Image
            style={{
              width: '100%',
              height: '100%',
              borderRadius: 10,
            }}
            source={{ uri: activity.image }}
          />
        </View>
      ))}
    </Swiper>
  )
}

export default ActivitySwiper;