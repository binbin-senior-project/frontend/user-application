import React from 'react';
import { View, Image } from 'react-native';
import Swiper from 'react-native-swiper';
import LinearGradient from 'react-native-linear-gradient';
import colors from '@utils/colors';

interface ImageSwiperProps {
  images: Array<string>;
}

const ImageSwiper = ({
  images,
}: ImageSwiperProps) => {
  return (
    <Swiper
      style={{ height: 250 }}
      dotColor={colors.iconColor}
      activeDotColor={colors.primaryColor}
    >
      {images.map(image => (
        <View style={{
          position: 'relative',
          flex: 1,
        }}>
          <LinearGradient
            colors={[
              'rgba(0, 0, 0, 0)',
              'rgba(0, 0, 0, 0.4)'
            ]}
            style={{
              position: 'absolute',
              width: '100%',
              height: '100%',
              zIndex: 2,
          }} />
          <Image
            source={{ uri: image }}
            style={{
              width: '100%',
              height: '100%'
            }}
          />
        </View>
      ))}
    </Swiper>
  )
}

export default ImageSwiper;