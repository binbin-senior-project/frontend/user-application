import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import messaging from '@react-native-firebase/messaging';
import auth from '@react-native-firebase/auth';
import { setTokenAction, setMessagingTokenAction } from '@actions/auth';
import { setProfileAction } from '@actions/user';
import LoaderDefault from '@components/loaders/LoaderDefault';

interface Props {
  setToken: Function;
  setMessagingToken: Function;
  setProfile: Function;
  navigation: any;
}

const AuthLoading = ({
  setToken,
  setMessagingToken,
  setProfile,
  navigation,
}: Props) => {
  const [_isLoading, _setLoading] = useState(true);

  const loadToken = async () => {
    try {
      const user = await auth().currentUser;

      messaging().requestPermission();

      if (user) {
        const accessToken = (await user.getIdToken()).toString();
        const messagingToken = (await messaging().getToken()).toString();

        await setToken(accessToken);
        setMessagingToken(accessToken, messagingToken);
        setProfile();

        _setLoading(false);
        navigation.navigate('Main');
        return;
      }
    } catch {
      _setLoading(false);
      return navigation.navigate('Register');
    }

    _setLoading(false);
    return navigation.navigate('Register');
  };

  useEffect(() => {
    loadToken();
  }, [navigation]);

  return <LoaderDefault isLoading={_isLoading} />;
};

const mapDispatchToProps = (dispatch: Function) => ({
  setToken: (accessToken: string) => dispatch(setTokenAction(accessToken)),
  setMessagingToken: (accessToken: string, token: string) =>
    dispatch(setMessagingTokenAction(accessToken, token)),
  setProfile: () => dispatch(setProfileAction()),
});

export default connect(
  null,
  mapDispatchToProps,
)(AuthLoading);
