import React, { useState, useEffect } from 'react';
import {Vibration, View, StatusBar } from 'react-native';
import { connect } from 'react-redux';
import TouchID from 'react-native-touch-id';
import Icon from 'react-native-vector-icons/Feather';
import colors from '@utils/colors';
import Passcode from '@components/Passcode';
import { H1, P } from '@components/Global';
import { useMutation } from '@apollo/react-hooks';
import { VERIFY_PASSCODE } from '@graphql/mutation';

interface Props {
  accessToken: string;
  onHideModal: Function;
  onComplete: Function;
}

const VerifyModal = ({ accessToken, onHideModal, onComplete }: Props) => {
  const [isTouchID, setTouchID] = useState(true);
  const [_passcode, _setPasscode] = useState([]);
  const [_verifyPasscode] = useMutation(VERIFY_PASSCODE);

  const onCompletePasscode = async () => {
    const { data } = await _verifyPasscode({
      variables: { input: { passcode: _passcode.join('') } },
      context: { headers: { authorization: `Bearer ${accessToken}` } },
    });

    if (data?.verifyPasscode?.success) {
      return onComplete();
    }

    Vibration.vibrate();
    _setPasscode([]);
  }

  useEffect(() => {
    if (isTouchID) {
      TouchID.authenticate('ยืนยัน')
        .then(() => onComplete())
        .catch((error: string) => {
          const result = error.toString();
          const isCancel = result.match('LAErrorUserCancel');
          const isFallback = result.match('LAErrorUserFallback');

          if (isCancel || isFallback) {
            setTouchID(false);
          }
        });
    }
  });

  return (
    <View
      style={{
        backgroundColor: colors.pageBackgroundColor,
        height: '100%',
        padding: 20,
        paddingTop: 100,
        position: 'relative',
      }}>
      <StatusBar barStyle="dark-content" />
      <Icon
        name="x"
        size={30}
        onPress={() => onHideModal()}
        style={{
          position: 'absolute',
          top: 80,
          right: 20,
        }}
      />
      <View style={{ flex: 1 }}>
        <H1 bold>ยืนยันรหัส PIN</H1>
        <P style={{ marginBottom: 20 }}>ปลดล็อคการทำรายการ</P>
        <Passcode passcode={_passcode} onPress={(passcode: any) => _setPasscode(passcode)} onComplete={onCompletePasscode} />
      </View>
    </View>
  );
};

const mapStateToProps = (state: any) => ({
  accessToken: state.auth.accessToken,
});

export default connect(mapStateToProps)(VerifyModal);
