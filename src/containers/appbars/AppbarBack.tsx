import React from 'react';
import { StatusBar } from 'react-native';
import { connect } from 'react-redux';
import { H1, Row } from '@components/Global';
import Icon from 'react-native-vector-icons/Feather';
import PointButton from '@components/buttons/PointButton';
import colors from '@utils/colors';

interface Props {
  title: string;
  user: any;
  showPoint?: boolean;
  navigation: any;
}

const AppbarBack = ({ title, user, showPoint = true, navigation }: Props) => {
  return (
    <Row
      between
      alignCenter
      style={{
        width: '100%',
        paddingHorizontal: 20,
        paddingTop: 45,
        paddingBottom: 10,
        backgroundColor: colors.primaryColor,
      }}>
      <StatusBar barStyle="light-content" />
      <Row alignCenter>
        <Icon
          name="arrow-left"
          size={25}
          color="#ffffff"
          style={{ marginRight: 10 }}
          onPress={() => navigation.pop()}
        />
        <H1 white style={{ marginLeft: 10 }}>
          {title}
        </H1>
      </Row>
      {showPoint && <PointButton point={user.point} onPress={() => {}} />}
    </Row>
  );
};

const mapStateToProps = (state: any) => ({
  user: state.user,
});

export default connect(mapStateToProps)(AppbarBack);
