import React from 'react';
import { connect } from 'react-redux';
import Appbar from '@components/appbars/Appbar';

interface Props {
  title: string;
  user: any;
  navigation: any;
}

const AppbarDefault = ({ title, user, navigation }: Props) => {
  return (
    <Appbar
      title={title}
      avatarImage={user.photo}
      onPressAvatar={() => navigation.navigate('Profile')}
      onPressPoint={() => navigation.navigate('Scan')}
      point={user.point || 0}
    />
  );
};

const mapStateToProps = (state: any) => ({
  user: state.user,
});

export default connect(mapStateToProps)(AppbarDefault);
