import React, { useState } from 'react';
import AppbarSearch from '@components/appbars/AppbarSearch';

interface Props {
  title: string;
  navigation: any;
}

const AppbarMap = ({ title, navigation }: Props) => {
  const [keyword, setKeyword] = useState('');
  const [isSearch, setSearch] = useState(false);

  const toggleSearch = () => {
    setSearch(!isSearch);
  };

  const onType = (text: string) => {
    setKeyword(text);
  };

  const onSubmit = () => {};

  const onBack = () => {
    navigation.pop();
  };

  return (
    <AppbarSearch
      title={title}
      keyword={keyword}
      placeholder="ค้นหาถัง Binbin"
      isSearch={isSearch}
      toggleSearch={toggleSearch}
      onType={onType}
      onSubmit={onSubmit}
      onBack={onBack}
    />
  );
};

export default AppbarMap;
