import React from 'react';
import { connect } from 'react-redux';
import AppbarSearch from '@components/appbars/AppbarSearch';

interface Props {
  title: string;
  user: any;
  navigation: any;
}

const AppbarStore = ({ title, user, navigation }: Props) => {
  return (
    <AppbarSearch
      title={title}
      avatarImage={user.photo}
      point={user.point}
      onPressAvatar={() => {
        navigation.navigate('Profile');
      }}
    />
  );
};

const mapStateToProps = (state: any) => ({
  user: state.user,
});

export default connect(mapStateToProps)(AppbarStore);
