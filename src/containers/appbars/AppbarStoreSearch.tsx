import React, { useState } from 'react';
import { TextInput, StatusBar } from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Feather';
import { Row, Col } from '@components/Global';
import colors from '@utils/colors';
import { setStoreSearchAction, resetSearchAction } from '@actions/search';

interface Props {
  keyword: string;
  setStoreSearch: Function;
  resetSearch: Function;
  navigation: any;
}

const AppbarStoreSearch = ({
  setStoreSearch,
  resetSearch,
  navigation,
}: Props) => {
  const [_search, setSearch] = useState('');

  const _onClose = () => {
    setStoreSearch('');
    navigation.pop();
  };

  const onSubmit = () => {
    setStoreSearch(_search);
  };

  return (
    <Row
      between
      alignCenter
      style={{
        width: '100%',
        height: 104,
        paddingHorizontal: 20,
        paddingTop: 35,
        backgroundColor: colors.primaryColor,
        borderBottomColor: colors.grayBorder,
        borderBottomWidth: 1,
      }}>
      <StatusBar barStyle="light-content" />
      <Row alignCenter>
        <Col>
          <Icon
            name="x"
            size={25}
            color={colors.whiteColor}
            style={{ marginRight: 10 }}
            onPress={() => _onClose()}
          />
        </Col>
        <Col style={{ flex: 1 }}>
          <TextInput
            autoFocus
            clearButtonMode="always"
            placeholder="ค้นหาร้านค้า"
            placeholderTextColor={colors.primaryColor}
            value={_search}
            onChangeText={text => setSearch(text)}
            onSubmitEditing={onSubmit}
            style={{
              width: '100%',
              paddingVertical: 5,
              paddingHorizontal: 15,
              borderRadius: 5,
              backgroundColor: colors.secondaryColor,
              fontFamily: 'DBHeavent',
              fontSize: 25,
            }}
          />
        </Col>
      </Row>
    </Row>
  );
};

const mapDispatchToProps = (dispatch: Function) => ({
  setStoreSearch: (keyword: string) => dispatch(setStoreSearchAction(keyword)),
  resetSearch: () => dispatch(resetSearchAction()),
});

export default connect(
  null,
  mapDispatchToProps,
)(AppbarStoreSearch);
