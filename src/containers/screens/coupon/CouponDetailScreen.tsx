import React, { useState, useEffect } from 'react';
import {
  View,
  Image,
  Modal,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import { connect } from 'react-redux';
import {  useMutation, useLazyQuery } from '@apollo/react-hooks';
import { Container, Row, H1, P, Col } from '@components/Global';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import LinearGradient from 'react-native-linear-gradient';
import RedeemButton from '@components/buttons/RedeemButton';
import Panel from '@components/panels/Panel';
import VerifyModal from '@containers/VerifyModal';
import colors from '@utils/colors';
import { GET_COUPON } from '@graphql/query';
import {
  REDEEM_COUPON,
  FAVORITE_COUPON,
  UNFAVORITE_COUPON,
} from '@graphql/mutation';
import Divider from '@components/Divider';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

interface Props {
  route: any;
  accessToken: string;
  navigation: any;
}

const CouponDetailScreen = ({ route, accessToken, navigation }: Props) => {
  const [_coupon, _setCoupon] = useState(null);
  const [isRedeem, setRedeem] = useState(false);
  const [_isLoading, _setLoading] = useState(true);
  const [_isFavorite, _setFavorite] = useState(false);
  const [_redeemCoupon] = useMutation(REDEEM_COUPON);
  const [_favoriteCoupon] = useMutation(FAVORITE_COUPON);
  const [_unfavoriteCoupon] = useMutation(UNFAVORITE_COUPON);

  const [getCoupon] = useLazyQuery(GET_COUPON, {
    fetchPolicy: 'no-cache',
    onCompleted: (data: any) => {
      _setLoading(false);

      if (data?.getCoupon == null) return;

      _setCoupon(data.getCoupon);
      _setFavorite(data?.getCoupon.isFavorite);
    }
  });

  const onFavorite = async (id: string) => {
    if (_coupon?.isFavorite) {
      _setFavorite(false);
      await _unfavoriteCoupon({
        variables: { input: { couponID: id } },
        context: { headers: { authorization: `Bearer ${accessToken}` } },
      });

    } else {
      _setFavorite(true);
      await _favoriteCoupon({
        variables: { input: { couponID: id } },
        context: { headers: { authorization: `Bearer ${accessToken}` } },
      });
    }

    getCoupon({
      variables: { couponID: route.params.couponID },
      context: { headers: { authorization: `Bearer ${accessToken}` } },
    });
  };

  const onVerifySucess = async () => {
    setRedeem(false);

    // Making redeem here
    const { data } = await _redeemCoupon({
      variables: { input: { couponID: route.params.couponID } },
      context: { headers: { authorization: `Bearer ${accessToken}` } },
    });

    if (data?.redeemCoupon?.success) {
      navigation.navigate('RedeemReceiptScreen');
    }
  };

  useEffect(() => {

    getCoupon({
      variables: { couponID: route.params.couponID },
      context: { headers: { authorization: `Bearer ${accessToken}` } },
    })

  }, [navigation])

  if (_isLoading) {
    return (
      <Container>
        <ActivityIndicator color={colors.primaryColor} />
      </Container>
    );
  }

  return (
    <Container disablePadding>
      <Modal animationType="slide" transparent={true} visible={isRedeem}>
        <VerifyModal
          onHideModal={() => setRedeem(false)}
          onComplete={onVerifySucess}
        />
      </Modal>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View
          style={{
            position: 'relative',
            width: '100%',
            height: 300,
            backgroundColor: colors.primaryColor,
          }}>
            <View style={{
              width: 40,
              height: 40,
              position: 'absolute',
              top: 20,
              right: 20,
              zIndex: 99,
              backgroundColor: colors.whiteColor,
              borderRadius: 20,
              alignItems: 'center',
              justifyContent: 'center'
            }}>
            <TouchableWithoutFeedback
              onPress={() => onFavorite(_coupon.id)}
            >
              <Col center>
                {_isFavorite ? (
                  <Icon
                    name="heart"
                    size={24}
                    color={colors.redColor}
                    style={{ lineHeight: 40}}
                  />
                ) : (
                  <Icon
                    name="heart-outline"
                    size={24}
                    style={{ lineHeight: 40}}
                  />
                )}
              </Col>
            </TouchableWithoutFeedback>
          </View>
          <LinearGradient
            colors={[
              'rgba(0, 0, 0, 0)',
              'rgba(0, 0, 0, 0.2)',
              'rgba(0, 0, 0, 0.5)',
            ]}
            style={{
              position: 'absolute',
              width: '100%',
              height: '100%',
              zIndex: 2,
            }}
          />
          <Image
            style={{
              width: '100%',
              height: '100%',
            }}
            resizeMode="cover"
            source={{
              uri: _coupon.photo,
            }}
          />
        </View>
        <View
          style={{
            alignItems: 'center',
            backgroundColor: colors.whiteColor,
            marginBottom: 10,
            borderBottomWidth: 1,
            borderColor: colors.inputBackgroundColor,
          }}>
          <H1 style={{ marginTop: 5 }}>{_coupon.store.name}</H1>
          <P style={{ marginTop: 5, marginBottom: 5 }}>{_coupon.name}</P>
          <Divider />
          <Row
            alignCenter
            style={{
              paddingHorizontal: 20,
              paddingTop: 10,
              paddingBottom: 20,
            }}>
            <RedeemButton point={_coupon.point} onPress={() => setRedeem(true)} />
          </Row>
        </View>
        <Panel title="รายละเอียด" description={_coupon.description} />
        <Panel title="เงื่อนไข" description={_coupon.condition} />
      </ScrollView>
    </Container>
  );
};

const mapStateToProps = (state: any) => ({
  accessToken: state.auth.accessToken,
});

export default connect(mapStateToProps)(CouponDetailScreen);
