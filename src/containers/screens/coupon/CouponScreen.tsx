import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { useQuery, useLazyQuery } from '@apollo/react-hooks';
import moment from 'moment';
import Tab from '@components/Tab';
import CouponList from '@components/CouponList';
import {
  GET_COUPON_CATEGORIES,
  GET_COUPONS,
  GET_COUPON_BY_CATEGORY,
} from '@graphql/query';

interface Props {
  accessToken: string;
  navigation: any;
}

const CouponScreen = ({ accessToken, navigation }: Props) => {
  const [_couponCategories, _setCouponCategories] = useState([
    { id: 'all', title: 'ทั้งหมด' },
  ]);
  const [_currentCategory, _setCurrentCategory] = useState('all');
  const [coupons, _setCoupons] = useState([]);
  const [_offset, _setOffset] = useState(0);
  const [isLoading, setLoading] = useState(false);

  const renderNewCoupon = (couponData: any) => {
    if (couponData == null) return;

    const coupons = couponData?.map((item: any) => {
      const d = Date.parse(item.expire.replace(/-/gi, '/').split('.')[0]);
      const expire = moment(d).locale('th');

      return {
        id: item.id,
        photo: item.photo,
        name: item.name,
        point: item.point,
        expire: expire.add(543, 'y').format('ll HH:mm'),
        store: item.store,
      };
    });

   return coupons;
  };

  // Get Types
  useQuery(GET_COUPON_CATEGORIES, {
    fetchPolicy: 'no-cache',
    errorPolicy: 'ignore',
    variables: { offset: 0, limit: 100 },
    context: { headers: { authorization: `Bearer ${accessToken}` } },
    onCompleted: (data: any) => {
      const newCouponCategories = data?.getCouponCategories?.map((item: any) => {
        return {
          id: item.id,
          title: item.name,
        };
      }) || [];

      _setCouponCategories([..._couponCategories, ...newCouponCategories]);
    },
  });

  // Get Coupons
  const [_getCoupons] = useLazyQuery(GET_COUPONS, {
    fetchPolicy: 'no-cache',
    errorPolicy: 'ignore',
    onCompleted: (data: any) => {
      setLoading(false);

      if (data?.getCoupons == null) return;

      const coupons = renderNewCoupon(data.getCoupons);
      _setCoupons(coupons);
      _setOffset(0);
    },
  });

   // Get Coupons
   const [_loadMoreCoupons] = useLazyQuery(GET_COUPONS, {
    fetchPolicy: 'no-cache',
    errorPolicy: 'ignore',
    context: { headers: { authorization: `Bearer ${accessToken}` } },
    onCompleted: (data: any) => {

      if (data?.getCoupons == null) return;

      const newCoupons = renderNewCoupon(data.getCoupons);
      _setCoupons([...coupons, ...newCoupons]);
    },
  });

  // Get Coupons By Type
  const [_getCouponsByCategory] = useLazyQuery(GET_COUPON_BY_CATEGORY, {
    fetchPolicy: 'no-cache',
    errorPolicy: 'ignore',
    onCompleted: (data: any) => {
      setLoading(false);

      if (data?.getCouponsByCategory == null) return;

      const coupons = renderNewCoupon(data?.getCouponsByCategory);
      _setCoupons(coupons);
      _setOffset(0);
    },
  });

  // Get Coupons By Type
  const [_loadMoreCouponsByCategory] = useLazyQuery(GET_COUPON_BY_CATEGORY, {
    fetchPolicy: 'no-cache',
    errorPolicy: 'ignore',
    onCompleted: (data: any) => {
      setLoading(false);

      if (data?.getCouponsByCategory == null) return;

      const newCoupons = renderNewCoupon(data?.getCouponsByCategory);
      _setCoupons([...coupons, ...newCoupons]);
    },
  });

  const onTabPress = (categoryID: string) => {
    if (_currentCategory == categoryID) return;
    
    _setOffset(0);
    _setCurrentCategory(categoryID);
  };

  const onEndReached = () => {
    _setOffset(_offset + 6);
  };

  const onRefresh = () => {
    if (_currentCategory == 'all') {
      _getCoupons({
        variables: { offset: 0, limit: 6 },
        context: { headers: { authorization: `Bearer ${accessToken}` } },
       });
      return;
    }

    _getCouponsByCategory({
      variables: { categoryID: _currentCategory, offset: 0, limit: 6 },
      context: { headers: { authorization: `Bearer ${accessToken}` } },
    });
  }

  useEffect(() => {
    setLoading(true);

    const unsubscribe = navigation.addListener('focus', () => {
      _getCoupons({
       variables: { offset: 0, limit: 6 },
       context: { headers: { authorization: `Bearer ${accessToken}` } },
      })
    });

    return unsubscribe;
  }, [navigation]);


  useEffect(() => {
    if (_offset == 0) return;

    if (_currentCategory == 'all') {
      _loadMoreCoupons({
        variables: { offset: _offset, limit: 6 },
        context: { headers: { authorization: `Bearer ${accessToken}` } },
      })
      return;
    }

    _loadMoreCouponsByCategory({
      variables: { categoryID: _currentCategory, offset: _offset, limit: 6 },
      context: { headers: { authorization: `Bearer ${accessToken}` } },
    })
  }, [_offset])


  useEffect(() => {
    if (_currentCategory == 'all') {
      _getCoupons({
        variables: { offset: 0, limit: 6 },
        context: { headers: { authorization: `Bearer ${accessToken}` } },
      });
      
      return
    }

    _getCouponsByCategory({
      variables: { categoryID: _currentCategory, offset: 0, limit: 6 },
      context: { headers: { authorization: `Bearer ${accessToken}` } },
    });
  }, [_currentCategory])

  useEffect(() => {
    const unsubscribe = navigation.addListener('blur', () => {
     _setOffset(0);
     _setCurrentCategory('all');
    });

    return unsubscribe;
  }, [navigation]);


  return (
    <>
      <Tab
        items={_couponCategories}
        onPress={onTabPress}
        onPressSearch={() => navigation.navigate('CouponSearch')}
      />
      <CouponList
        isLoading={isLoading}
        coupons={coupons}
        onEndReached={onEndReached}
        onRefresh={onRefresh}
      />
    </>
  );
};

const mapStateToProps = (state: any) => ({
  accessToken: state.auth.accessToken,
})

export default connect(mapStateToProps)(CouponScreen);
