import React, { useState, useEffect } from 'react';
import { ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { useLazyQuery } from '@apollo/react-hooks';
import moment from 'moment';
import CouponList from '@components/CouponList';
import { GET_COUPON_BY_SEARCH } from '@graphql/query';
import { H3, Row } from '@components/Global';
import colors from '@utils/colors';

interface Props {
  accessToken: string;
  keyword: string;
}

const CouponSearch = ({ accessToken, keyword }: Props) => {
  const [coupons, setCoupons] = useState([]);
  const [offset, setOffset] = useState(0);
  const [isLoading, setLoading] = useState(false);

  const renderNewCoupon = (couponData: any) => {
    if (couponData == null) return;

    const newCoupons = couponData.map((item: any) => {
      const d = Date.parse(item.expire.replace(/-/gi, '/').split('.')[0]);
      const expire = moment(d).locale('th');

      return {
        id: item.id,
        photo: item.photo,
        name: item.name,
        point: item.point,
        expire: expire.add(543, 'y').format('ll HH:mm'),
        store: item.store,
      };
    });

    setCoupons([...coupons, ...newCoupons]);
  };

  const onEndReached = () => {
    setOffset(offset + 6);
  };

  const [getCouponsByearch] = useLazyQuery(GET_COUPON_BY_SEARCH, {
    fetchPolicy: 'no-cache',
    onCompleted: (data: any) => {
      setLoading(false);
      renderNewCoupon(data.getCouponsBySearch);
    },
  });

  useEffect(() => {
    setCoupons([]);
    setOffset(0);
    setLoading(true);
  }, [keyword]);

  useEffect(() => {
    getCouponsByearch({
      variables: { keyword, offset, limit: 6 },
      context: { headers: { authorization: `Bearer ${accessToken}` } },
    });
  }, [keyword, offset]);

  return (
    <ScrollView
      contentContainerStyle={{
        minHeight: '100%',
      }}>
      {keyword != '' && (
        <>
          <Row
            style={{
              backgroundColor: colors.whiteColor,
              paddingHorizontal: 20,
              paddingVertical: 20,
              borderBottomWidth: 1,
              borderBottomColor: colors.inputBackgroundColor,
            }}>
            <H3 bold>{`ผลการค้นหา: ${keyword}`}</H3>
          </Row>
          <CouponList
            isLoading={isLoading}
            coupons={coupons}
            onEndReached={onEndReached}
            scrollEnabled={false}
          />
        </>
      )}
    </ScrollView>
  );
};

const mapStateToProps = (state: any) => ({
  accessToken: state.auth.accessToken,
  keyword: state.search.coupon,
});

export default connect(mapStateToProps)(CouponSearch);
