import React, { useState, useEffect } from 'react';
import { ActivityIndicator, StatusBar } from 'react-native';
import { connect } from 'react-redux';
import moment from 'moment';
import {
  Container,
  Footer,
  Content,
  Col,
  H1,
  H3,
  Row,
  P,
  Center,
} from '@components/Global';
import Logo from '@components/Logo';
import Divider from '@components/Divider';
import Button from '@components/buttons/Button';
import { useLazyQuery } from '@apollo/react-hooks';
import { GET_TRANSACTIONS } from '@graphql/query';
import { updateLatestPointAction } from '@actions/user';
import colors from '@utils/colors';

interface Props {
  accessToken: string;
  updateLatestPoint: Function;
  navigation: any;
}

const RedeemReceiptScreen = ({ accessToken, updateLatestPoint, navigation }: Props) => {
  const [_transaction, _setTransaction] = useState({});
  const [_isLoading, _setLoading] = useState(true);

  const [getTransactions] = useLazyQuery(GET_TRANSACTIONS, {
    fetchPolicy: 'no-cache',
    onCompleted: (data: any) => {
      _setLoading(false);

      const { id, point, description, createdAt } = data?.getTransactions[0];

      const d = Date.parse(createdAt.replace(/-/gi, '/').split('.')[0]);
      const dd = moment(d).locale('th');
  
      _setTransaction({
        id,
        point,
        description,
        date: dd.add(543, 'y').format('ll'),
        time: dd.format('HH:mm')
      })

      updateLatestPoint();
    }
  });

  useEffect(() => {

    getTransactions({
      variables: { offset: 0, limit: 1 },
      context: { headers: { authorization: `Bearer ${accessToken}` } },
    })

  }, [navigation])

  if (_isLoading) {
    return (
      <Container>
        <Center>
          <ActivityIndicator color={colors.primaryColor} />
        </Center>
      </Container>
    );
  }

  return (
  <Container>
    <StatusBar barStyle="dark-content" />
    <Content style={{ justifyContent: 'center' }}>
      <Col center>
        <Logo width={100} />
        <H1>สำเร็จ</H1>
        <H3>แลกคูปองเรียบร้อย</H3>
      </Col>
      <Divider />
      <Row between>
        <Content>
            <P bold>เลขรายการ</P>
            <P>{_transaction.id}</P>
          </Content>
          <Content>
            <P bold right>
              จำนวนแต้ม
            </P>
            <P right>{_transaction.point}</P>
          </Content>
      </Row>
      <Divider />
      <Row between>
        <Content>
            <P bold>วันที่</P>
            <P>{_transaction.date}</P>
          </Content>
          <Content>
            <P bold right>
              เวลา
            </P>
            <P right>{_transaction.time}</P>
          </Content>
      </Row>
      <Divider />
      <Row between>
      <Content>
            <P bold>รายละเอียด</P>
            <P>{_transaction.description}</P>
          </Content>
      </Row>
      <Divider />
    </Content>
    <Footer>
      <Button text="เรียบร้อย" onPress={() => navigation.popToTop()} />
    </Footer>
  </Container>
  );
}

const mapStateToProps = (state: any) => ({
  accessToken: state.auth.accessToken,
});

const mapDispatchToProps = (dispatch: Function) => ({
  updateLatestPoint: () => dispatch(updateLatestPointAction()),
});


export default connect(mapStateToProps, mapDispatchToProps)(RedeemReceiptScreen);
