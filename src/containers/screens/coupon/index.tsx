import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import AppbarBack from '@containers/appbars/AppbarBack';
import AppbarCouponSearch from '@containers/appbars/AppbarCouponSearch';
import AppbarDefault from '@containers/appbars/AppbarDefault';
import CouponScreen from '@containers/screens/coupon/CouponScreen';
import CouponSearch from '@containers/screens/coupon/CouponSearch';
import CouponDetailScreen from '@containers/screens/coupon/CouponDetailScreen';
import StoreDetailScreen from '@containers/screens/store/StoreDetailScreen';
import RedeemReceiptScreen from './RedeemReceiptScreen';

const Stack = createStackNavigator();

const RegisterStack = () => {
  return (
    <Stack.Navigator initialRouteName="CouponScreen" headerMode="screen">
      <Stack.Screen
        name="CouponScreen"
        component={CouponScreen}
        options={{
          header: props => <AppbarDefault title="คูปอง" {...props} />,
        }}
      />
      <Stack.Screen
        name="CouponSearch"
        component={CouponSearch}
        options={{
          header: props => <AppbarCouponSearch {...props} />,
          animationEnabled: false,
        }}
      />
      <Stack.Screen
        name="CouponDetailScreen"
        component={CouponDetailScreen}
        options={{
          header: props => <AppbarBack title="คูปอง" {...props} />,
        }}
      />
      <Stack.Screen
        name="StoreDetailScreen"
        component={StoreDetailScreen}
        options={{
          header: props => <AppbarBack title="ร้านค้า" {...props} />,
        }}
      />
      <Stack.Screen
        name="RedeemReceiptScreen"
        component={RedeemReceiptScreen}
        options={{
          header: () => null,
        }}
      />
    </Stack.Navigator>
  );
};

export default RegisterStack;
