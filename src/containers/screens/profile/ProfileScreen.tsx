import React from 'react';
import { View, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { CommonActions } from '@react-navigation/native';
import PointCard from '@components/PointCard';
import HighlightButton from '@components/buttons/HighlightButton';
import Panel from '@components/panels/Panel';
import ListItem from '@components/ListItem';
import { Container } from '@components/Global';
import { logoutAction } from '@actions/auth';
import colors from '@utils/colors';

interface Props {
  user: any;
  logout: Function;
  navigation: any;
}

const ProfileScreen = ({ user, logout, navigation }: Props) => {
  const onLogout = () => {
    logout();

    navigation.dispatch(
      CommonActions.reset({
        routes: [{ name: 'Register' }],
      }),
    );
  };

  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <Container disablePadding>
        <PointCard point={user.point} />
        <Panel>
          <ListItem
            icon="bookmark"
            title="คูปองที่ถูกใจ"
            onPress={() => {
              navigation.push('FavoriteCouponStack');
            }}
          />
          <ListItem
            icon="gift"
            title="มอบแต้มเป็นของขวัญ"
            onPress={() => {
              navigation.push('TransferStack');
            }}
          />
          <ListItem
            icon="tag"
            title="ราคาขวดวันนี้"
            onPress={() => {
              navigation.push('BottleStack');
            }}
          />
          <ListItem
            icon="search"
            title="ค้นหาถัง Binbin"
            onPress={() => {
              navigation.push('MapStack');
            }}
          />
          <ListItem
            icon="settings"
            title="ตั้งค่าโปรไฟล์"
            onPress={() => {
              navigation.push('SettingStack');
            }}
          />
          {/* <ListItem
            icon="info"
            title="คำถามที่พบบ่อย"
            onPress={() => {
              navigation.push('ContactStack');
            }}
          /> */}
        </Panel>
        <Panel>
          <View
            style={{
              padding: 20,
              borderBottomWidth: 1,
              borderBottomColor: colors.grayBorder,
            }}>
            <HighlightButton onPress={onLogout} text="ออกจากระบบ" />
          </View>
        </Panel>
      </Container>
    </ScrollView>
  );
};

const mapStateToProps = (state: any) => ({
  user: state.user,
});

const mapDispatchToProps = (dispatch: Function) => ({
  logout: () => dispatch(logoutAction()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProfileScreen);
