import React from 'react';
import { View, ScrollView, ActivityIndicator } from 'react-native';
import {connect} from 'react-redux'; 
import { Container, Content, Row, P } from '@components/Global';
import PanelTransparent from '@components/panels/PanelTransparent';
import Coin from '@components/Coin';
import Divider from '@components/Divider';
import colors from '@utils/colors';
import { GET_TRASHES } from '@graphql/query';
import { useQuery } from '@apollo/react-hooks';

interface Props {
  accessToken: string;
  navigation: any;
}

const BottleScreen = ({ accessToken, navigation }: Props) => {
  const { loading, data } = useQuery(GET_TRASHES, {
    variables: { offset: 0 ,limit: 100 },
    context: { headers: { authorization: `Bearer ${accessToken}` } },
  });

  if (loading) {
    return (
      <ActivityIndicator
        color={colors.primaryColor}
        style={{ marginVertical: 40 }}
      />
    );
  }

  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <Container>
        <Content>
          <PanelTransparent title="ราคาขวดวันนี้">
            {data?.getTrashes?.map((trash: any) => (
              <>
                <Row between style={{ paddingVertical: 5 }}>
                  <Content>
                    <P>
                      {trash.name} - {trash.code}
                    </P>
                  </Content>
                  <Content>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'flex-end',
                      }}>
                      <P right style={{ marginRight: 5 }}>
                        {trash.regularPoint}
                      </P>
                      <Coin />
                    </View>
                  </Content>
                </Row>
                <Divider />
              </>
            ))}
          </PanelTransparent>
        </Content>
      </Container>
    </ScrollView>
  );
};

const mapStateToProps = (state: any) => ({
  accessToken: state.auth.accessToken,
})

export default connect(mapStateToProps)(BottleScreen);