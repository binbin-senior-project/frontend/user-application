import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import AppbarBack from '@containers/appbars/AppbarBack';
import BottleScreen from './BottleScreen';

const Stack = createStackNavigator();

const Bottle = () => {
  return (
    <Stack.Navigator initialRouteName="BottleScreen" headerMode="screen">
      <Stack.Screen
        name="BottleScreen"
        component={BottleScreen}
        options={{
          header: (props) => (
            <AppbarBack title="ราคาขวดวันนี้" {...props} showPoint={false} />
          ),
        }}
      />
    </Stack.Navigator>
  );
};

export default Bottle;
