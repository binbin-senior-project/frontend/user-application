import React from 'react'
import { KeyboardAvoidingView } from 'react-native';
import {
  Container,
  Footer,
  Content,
} from '@components/Global';
import PanelTransparent from '@components/panels/PanelTransparent';
import Textarea from '@components/forms/Textarea';
import Button from '@components/buttons/Button';

interface Props {
  navigation: any,
}

const ContactSubmitScreen = ({ navigation }: Props) => {
  return (
    <KeyboardAvoidingView
      behavior="padding"
      keyboardVerticalOffset={100}
    >
      <Container>
        <Content>
          <PanelTransparent title="เขียนคำถาม">
            <Textarea placeholder="รายละเอียด"/>
          </PanelTransparent>
        </Content>
        <Footer>
          <Button
            showNextIcon
            text="ถัดไป"
            onPress={() => navigation.navigate('ContactReceiptScreen')}
          />
        </Footer>
      </Container>
    </KeyboardAvoidingView>
  );
}

export default ContactSubmitScreen;