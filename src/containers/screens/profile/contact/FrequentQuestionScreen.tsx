import React from 'react';
import { ScrollView } from 'react-native';
import { Container } from '@components/Global';
import ListItem from '@components/ListItem';
import Panel from '@components/panels/Panel';

interface Props {
  navigation: any;
}

const FrequentQuestionScreen = ({ navigation }: Props) => {
  return (
    <ScrollView>
      <Container disablePadding style={{ paddingTop: 10 }}>
        <Panel>
          <ListItem
            title="ไม่สามารถสร้างคูปองได้"
            onPress={() => {
              navigation.navigate('FrequentQuestionSingleScreen');
            }}
          />
        </Panel>
      </Container>
    </ScrollView>
  );
};

export default FrequentQuestionScreen;
