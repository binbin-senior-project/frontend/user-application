import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { useLazyQuery } from '@apollo/react-hooks';
import moment from 'moment';
import CouponList from '@components/CouponList';
import { GET_FAVORITE_COUPONS } from '@graphql/query';

interface Props {
  route: any;
  accessToken: string;
  navigation: any;
}

const FavoriteCouponScreen = ({ route, accessToken, navigation }: Props) => {
  const [coupons, setCoupons] = useState([]);
  const [offset, setOffset] = useState(0);
  const [isLoading, setLoading] = useState(true);

  const renderNewCoupon = (couponData: any) => {
    if (couponData == null) return;

    const newCoupons = couponData.map((item: any) => {
      const d = Date.parse(item.expire.replace(/-/gi, '/').split('.')[0]);
      const expire = moment(d).locale('th');

      return {
        id: item.id,
        photo: item.photo,
        name: item.name,
        point: item.point,
        expire: expire.add(543, 'y').format('ll HH:mm'),
        store: item.store,
      };
    });

    setCoupons([...coupons, ...newCoupons]);
  };

  // Get Coupons
  const [getFavoriteCoupons] = useLazyQuery(GET_FAVORITE_COUPONS, {
    fetchPolicy: 'no-cache',
    onCompleted: (data: any) => {
      renderNewCoupon(data.getFavoriteCoupons);
      setLoading(false);
    },
  });

  const onEndReached = () => {
    setOffset(offset + 6);
  };

  useEffect(() => {
    getFavoriteCoupons({
      variables: { offset, limit: 6 },
      context: { headers: { authorization: `Bearer ${accessToken}` } },
    });
  }, [offset, navigation, route]);

  return (
    <CouponList
      isLoading={isLoading}
      coupons={coupons}
      onEndReached={onEndReached}
    />
  );
};

const mapStateToProps = (state: any) => ({
  accessToken: state.auth.accessToken,
});

export default connect(mapStateToProps)(FavoriteCouponScreen);
