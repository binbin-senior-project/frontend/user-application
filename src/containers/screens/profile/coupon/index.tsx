import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import AppbarBack from '@containers/appbars/AppbarBack';
import FavoriteCouponScreen from './FavoriteCoupon';
import CouponDetailScreen from '@containers/screens/coupon/CouponDetailScreen';
import RedeemReceiptScreen from '@containers/screens/coupon/RedeemReceiptScreen';

const Stack = createStackNavigator();

const Coupon = () => {
  return (
    <Stack.Navigator
      initialRouteName="FavoriteCouponScreen"
      headerMode="screen">
      <Stack.Screen
        name="FavoriteCouponScreen"
        component={FavoriteCouponScreen}
        options={{
          header: props => (
            <AppbarBack title="คูปองที่ถูกใจ" {...props} showPoint={false} />
          ),
        }}
      />
      <Stack.Screen
        name="CouponDetailScreen"
        component={CouponDetailScreen}
        options={{
          header: props => <AppbarBack title="คูปอง" {...props} />,
        }}
      />
      <Stack.Screen
        name="RedeemReceiptScreen"
        component={RedeemReceiptScreen}
        options={{
          header: () => null,
        }}
      />
    </Stack.Navigator>
  );
};

export default Coupon;
