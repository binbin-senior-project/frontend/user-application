import React, { useState } from 'react';
import { Modal, KeyboardAvoidingView } from 'react-native';
import Input from '@components/forms/Input';
import Button from '@components/buttons/Button';
import { Container, Content, Footer } from '@components/Global';
import PanelTransparent from '@components/panels/PanelTransparent';
import VerifyModal from '@containers/VerifyModal';

interface Props {
  navigation: any;
}

const ExchangeStep1 = ({ navigation }: Props) => {
  const [isExchange, setExchange] = useState(false);
  const [isLoading, setLoading] = useState(false);

  const onExchangeCancel = () => {
    setExchange(false);
  }

  const onVerifySucess = () => {
    setExchange(false);
    setLoading(true);

    setTimeout(() => {
      setLoading(false);
      navigation.navigate('ExchangeStep2')
    }, 1000)
  };

  return (
    <KeyboardAvoidingView
      behavior="padding"
      keyboardVerticalOffset={100}
    >
      <Container>
        <Modal
          animationType="slide"
          transparent={true}
          visible={isExchange}
        >
          <VerifyModal
            onHideModal={onExchangeCancel}
            onComplete={onVerifySucess}
          />
        </Modal>
        <Content>
          <PanelTransparent title="เลือกบัญชีที่ต้องการโอน">
            <Input
              icon="credit-card"
              placeholder="ชื่อบัญชี"
            />
            <Input
              icon="credit-card"
              placeholder="หมายเลขบัญชี"
            />
            <Input
              icon="credit-card"
              placeholder="ธนาคาร"
            />
            <Input
              icon="dollar-sign"
              placeholder="จำนวนแต้มที่ต้องการแลกเปลี่ยน"
            />
          </PanelTransparent>
        </Content>
        <Footer>
          <Button
            showNextIcon
            text="ถัดไป"
            onPress={() => setExchange(true)}
          />
        </Footer>
      </Container>
    </KeyboardAvoidingView>
  );
};

export default ExchangeStep1;
