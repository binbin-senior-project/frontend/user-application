import React from 'react';
import { StatusBar } from 'react-native';
import {
  Container,
  Footer,
  Content,
  Column,
  H1,
  H3,
  Row,
  P,
} from '@components/Global';
import Logo from '@components/Logo';
import Divider from '@components/Divider';
import Button from '@components/buttons/Button';

interface Props {
  navigation: any,
}

const ExchangeStep2 = ({ navigation }: Props) => (
  <Container>
    <StatusBar barStyle="dark-content" />
    <Content style={{ justifyContent: 'center' }}>
      <Column center>
        <Logo width={100}/>
        <H1>สำเร็จ</H1>
        <H3>แลกเปลี่ยนแต้มเรียบร้อย</H3>
      </Column>
      <Divider />
      <Row between>
        <Content>
          <P bold>เลขรายการ</P>
          <P>912532411500775</P>
        </Content>
        <Content>
          <P bold right>แต้มคงเหลือ</P>
          <P right>10.00</P>
        </Content>
      </Row>
      <Divider />
      <Row between>
        <Content>
          <P bold>วันที่</P>
          <P>20 มิถุนายน 2562</P>
        </Content>
        <Content>
          <P bold right>เวลา</P>
          <P right>11:26</P>
        </Content>
      </Row>
      <Divider />
      <Row between>
        <Content>
          <P bold>ประเภทบัญชี</P>
          <P>ธนาคารไทยพาณิชย์</P>
        </Content>
        <Content>
          <P bold right>จำนวน</P>
          <P right>10 บาท</P>
        </Content>
      </Row>
      <Divider />
      <Row between>
        <Content>
          <P bold>แลกไปที่</P>
          <P>914-202-2204</P>
        </Content>
      </Row>
      <Divider />
    </Content>
    <Footer>
      <Button
        text="เรียบร้อย"
        onPress={() => navigation.navigate('Profile')}
      />
    </Footer>
  </Container>
);

export default ExchangeStep2;
