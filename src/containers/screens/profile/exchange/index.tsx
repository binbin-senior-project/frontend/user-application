import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import AppbarBack from '@containers/appbars/AppbarBack';
import ExchangeStep1 from './ExchangeStep1';
import ExchangeStep2 from './ExchangeStep2';

const Stack = createStackNavigator();

const ExchangeStack = () => {
  return (
    <Stack.Navigator
      initialRouteName="ExchangeStep1"
      headerMode="screen"
    >
      <Stack.Screen
        name="ExchangeStep1"
        component={ExchangeStep1}
        options={{
          header: props => <AppbarBack title="แลกเปลี่ยน" {...props} showPoint={false} />,
        }}
      />
      <Stack.Screen
        name="ExchangeStep2"
        component={ExchangeStep2}
        options={{
          header: () => null,
        }}
      />
    </Stack.Navigator>
  );
}

export default ExchangeStack;