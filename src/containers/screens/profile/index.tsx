import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import AppbarDefault from '@containers/appbars/AppbarDefault';
import ProfileScreen from '@containers/screens/profile/ProfileScreen';
import FavoriteCouponStack from './coupon';
import TransferStack from './transfer';
import MapStack from './map';
import ContactStack from './contact';
import ExchangeStack from './exchange';
import SettingStack from './setting';
import BottleStack from './bottle';

const Stack = createStackNavigator();

const ProfileStack = () => {
  return (
    <Stack.Navigator initialRouteName="Profile" headerMode="screen">
      <Stack.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          header: (props) => <AppbarDefault title="โปรไฟล์" {...props} />,
        }}
      />
      <Stack.Screen
        name="FavoriteCouponStack"
        component={FavoriteCouponStack}
        options={{
          header: () => null,
        }}
      />
      <Stack.Screen
        name="TransferStack"
        component={TransferStack}
        options={{
          header: () => null,
        }}
      />
      <Stack.Screen
        name="ExchangeStack"
        component={ExchangeStack}
        options={{
          header: () => null,
        }}
      />
      <Stack.Screen
        name="BottleStack"
        component={BottleStack}
        options={{
          header: () => null,
        }}
      />
      <Stack.Screen
        name="MapStack"
        component={MapStack}
        options={{
          header: () => null,
        }}
      />
      <Stack.Screen
        name="SettingStack"
        component={SettingStack}
        options={{
          header: () => null,
        }}
      />
      <Stack.Screen
        name="ContactStack"
        component={ContactStack}
        options={{
          header: () => null,
        }}
      />
    </Stack.Navigator>
  );
};

export default ProfileStack;
