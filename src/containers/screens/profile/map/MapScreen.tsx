import React, { useState, useEffect } from 'react';
import { View, Image, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { useLazyQuery } from '@apollo/react-hooks';
import MapView, { Marker } from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import BinMarker from '@images/marker/bin.png';
import colors from '@utils/colors';
import { GET_BINS } from '@graphql/query';

interface Props {
  accessToken: string;
  navigation: any;
}

const MapScreen = ({ accessToken, navigation }: Props) => {
  const [_latitude, _setLatitude] = useState(0);
  const [_longitude, _setLongitude] = useState(0);
  const [_isLoading, _setLoading] = useState(false);
  const [_bins, _setBins] = useState([]);
  const [_getBins] = useLazyQuery(GET_BINS, {
    fetchPolicy: 'no-cache',
    onCompleted: (data: any) => {
      _setLoading(false);

      if (data?.getBins == null) return;

      _setBins(data?.getBins);
    }
  });

  useEffect(() => {

    const unsubscribe = navigation.addListener('focus', () => {

      _setLoading(true);

      _getBins({
        variables: { offset: 0 ,limit: 100 },
        context: { headers: { authorization: `Bearer ${accessToken}` } },
      })

      Geolocation.getCurrentPosition((position: Object) => {
        _setLatitude(position?.coords?.latitude);
        _setLongitude(position?.coords?.longitude);
      });
  
    });

    return unsubscribe;
  }, [navigation]);


  if (_isLoading) {
    return (
      <ActivityIndicator
        color={colors.primaryColor}
        style={{ marginVertical: 40 }}
      />
    );
  }

  return (
    <View>
      <MapView
        initialRegion={{
          latitude: _latitude,
          longitude: _longitude,
          latitudeDelta: 0.009,
          longitudeDelta: 0.009,
        }}
        style={{
          width: '100%',
          height: '100%',
        }}>
        <Marker
          coordinate={{
            latitude: _latitude,
            longitude: _longitude,
          }}>
          <View
            style={{
              position: 'relative',
              backgroundColor: colors.primaryColor,
              width: 20,
              height: 20,
              borderRadius: 50,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View
              style={{
                backgroundColor: 'rgba(62, 157, 26, 0.2)',
                width: 50,
                height: 50,
                borderRadius: 50,
              }}></View>
          </View>
        </Marker>
        {_bins?.map((bin: any) => (
          <Marker
            key={bin.id}
            coordinate={{
              latitude: bin.latitude,
              longitude: bin.longitude,
            }}>
            <Image source={BinMarker} />
          </Marker>
        ))}
      </MapView>
    </View>
  );
};

const mapStateToProps = (state: any) => ({
  accessToken: state.auth.accessToken,
})

export default connect(mapStateToProps)(MapScreen);