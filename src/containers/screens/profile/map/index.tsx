import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import MapScreen from './MapScreen';
import AppbarBack from '@containers/appbars/AppbarBack';

const Stack = createStackNavigator();

const MapStack = () => {
  return (
    <Stack.Navigator initialRouteName="MapScreen" headerMode="screen">
      <Stack.Screen
        name="MapScreen"
        component={MapScreen}
        options={{
          header: (props) => <AppbarBack title="แผนที่" {...props} />,
        }}
      />
    </Stack.Navigator>
  );
};

export default MapStack;
