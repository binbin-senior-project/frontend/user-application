import React, { useState, useEffect } from 'react';
import { View, KeyboardAvoidingView } from 'react-native';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
import storage from '@react-native-firebase/storage';
import uuid from 'react-native-uuid';
import { Container, Content, Footer, Center } from '@components/Global';
import PanelTransparent from '@components/panels/PanelTransparent';
import Input from '@components/forms/Input';
import Button from '@components/buttons/Button';
import UploadButton from '@components/buttons/UploadButton';
import { UPDATE_USER } from '@graphql/mutation';
import { useMutation, useLazyQuery } from '@apollo/react-hooks';
import { GET_USER } from '@graphql/query';
import LoaderDefault from '@components/loaders/LoaderDefault';
import { setProfileAction } from '@actions/user';

interface Props {
  accessToken: string;
  user: any;
  setProfile: Function;
  navigation: any;
}

const SettingScreen = ({
  accessToken,
  user,
  setProfile,
  navigation,
}: Props) => {
  const [_avatar, setAvatar] = useState(user.photo);
  const [_firstname, _setFirstname] = useState(user.firstname);
  const [_lastname, _setLastname] = useState(user.lastname);
  const [_isloading, setLoading] = useState(false);
  const [_updateUser] = useMutation(UPDATE_USER, {
    onCompleted: () => {
      setProfile();
    },
  });

  const onSelectImage = () => {
    const options = {
      title: 'เลือกรูปภาพ',
      maxWidth: 300,
      maxHeight: 300,
    };

    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel || response.error) {
        return;
      }

      setAvatar(response.uri);
    });
  };

  const onSubmit = async () => {
    setLoading(true);

    let photo = _avatar;

    if (_avatar != '' && !_avatar.includes('https')) {
      const photoName = uuid.v4();
      const reference = storage().ref(photoName);
      await reference.putFile(_avatar);

      photo = await storage()
        .ref(photoName)
        .getDownloadURL();
    }

    await _updateUser({
      variables: {
        input: { firstname: _firstname, lastname: _lastname, photo },
      },
      context: { headers: { authorization: `Bearer ${accessToken}` } },
    });

    setLoading(false);
  };

  return (
    <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={100}>
      <LoaderDefault isLoading={_isloading} />
      <Container>
        <Content>
          <PanelTransparent title="ตั้งค่าโปรไฟล์">
            <Center>
              <UploadButton avatar={_avatar} onPress={onSelectImage} />
            </Center>
            <View style={{ height: 200 }}>
              <Input
                placeholder="ชื่อ"
                value={_firstname}
                onChangeText={(text: string) => _setFirstname(text)}
              />
              <Input
                placeholder="นามสกุล"
                value={_lastname}
                onChangeText={(text: string) => _setLastname(text)}
              />
            </View>
          </PanelTransparent>
        </Content>
        <Footer>
          <Button text="อัปเดต" onPress={() => onSubmit()} />
        </Footer>
      </Container>
    </KeyboardAvoidingView>
  );
};

const mapStateToProps = (state: any) => ({
  accessToken: state.auth.accessToken,
  user: state.user,
});

const mapDispatchToProps = (dispatch: Function) => ({
  setProfile: () => dispatch(setProfileAction()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SettingScreen);
