import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import AppbarBack from '@containers/appbars/AppbarBack';
import SettingScreen from './SettingScreen';

const Stack = createStackNavigator();

const Setting = () => {
  return (
    <Stack.Navigator
      initialRouteName="SettingScreen"
      headerMode="screen"
    >
      <Stack.Screen
        name="SettingScreen"
        component={SettingScreen}
        options={{
          header: props => <AppbarBack title="ตั้งค่าโปรไฟล์" {...props} showPoint={false} />,
        }}
      />
    </Stack.Navigator>
  );
}

export default Setting;