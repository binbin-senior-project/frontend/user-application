import React, { useState, useEffect } from 'react';
import { KeyboardAvoidingView, Modal } from 'react-native';
import { connect } from 'react-redux';
import Input from '@components/forms/Input';
import Button from '@components/buttons/Button';
import { Container, Footer, Content } from '@components/Global';
import PanelTransparent from '@components/panels/PanelTransparent';
import LoaderDefault from '@components/loaders/LoaderDefault';
import { GET_USER, GET_USER_BYPHONE } from '@graphql/query';
import { useLazyQuery } from '@apollo/react-hooks';
import Error from '@components/Error';
import validator from 'validator';

interface Props {
  accessToken: string;
  user: any;
  navigation: any;
}

const TransferStep1 = ({ accessToken, user, navigation }: Props) => {
  const [_phone, _setPhone] = useState('')
  const [_point, _setPoint] = useState(0);
  const [_isLoading, _setLoading] = useState(false);
  const [_isError, _setError] = useState(false);
  const [_errorMsg, _setErrorMsg] = useState('');

  const [getUserByPhone] = useLazyQuery(GET_USER_BYPHONE, {
    fetchPolicy: 'no-cache',
    onCompleted: (data: any) => {
      _setLoading(false);

      if (data?.getUserByPhone == null || data?.getUserByPhone.firstname == "") {
        _setError(true);
        _setErrorMsg("ไม่มีผู้ใช้นี้")
        return;
      };

      navigation.navigate('TransferStep2',  {
        phone: _phone,
        point: _point,
        receiver: data?.getUserByPhone,
      })
    }
  })

  const onNext = () => {
    _setLoading(true);

    if (user.phone == _phone) {
      _setLoading(false);
      _setError(true);
      _setErrorMsg("ไม่สามารถมอบแต้มให้ตัวเองได้");
      return;
    }

    if (!validator.isMobilePhone(_phone)) {
      _setLoading(false);
      _setError(true);
      _setErrorMsg("ระบุเบอร์มือถือให้ถูกต้อง");
      return;
    }

    if (_point == 0) {
      _setLoading(false);
      _setError(true);
      _setErrorMsg("ระบุจำนวนแต้ม");
      return;
    }

    if (user.point < _point) {
      _setLoading(false);
      _setError(true);
      _setErrorMsg("คุณมีแต้มไม่เพียงพอ")
      return;
    }

    getUserByPhone({
      variables: { phone: _phone },
      context: { headers: { authorization: `Bearer ${accessToken}` }}
    })
  }

  return (
    <KeyboardAvoidingView
      behavior="padding"
      keyboardVerticalOffset={100}
    >
      <LoaderDefault isLoading={_isLoading} />
      <Error
        message={_errorMsg}
        isError={_isError}
        onPress={() => _setError(false)}
      />
      <Container>
        <Content>
          <PanelTransparent title="เลือกบัญชีที่ต้องการโอน">
            <Input
							type="number"
              placeholder="เบอร์มือถือ"
              value={_phone}
              onChangeText={_setPhone}
            />
            <Input
              placeholder="จำนวนแต้มที่โอน"
              value={_point}
              onChangeText={_setPoint}
            />
          </PanelTransparent>
        </Content>
        <Footer>
          <Button
            showNextIcon
            text="ถัดไป"
            onPress={() => onNext()}
          />
        </Footer>
      </Container>
    </KeyboardAvoidingView>
  );
};

const mapStateToProps = (state: any) => ({
  accessToken: state.auth.accessToken,
  user: state.user,
})

export default connect(mapStateToProps)(TransferStep1);
