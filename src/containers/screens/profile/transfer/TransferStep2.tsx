import React, { useState } from 'react';
import { StatusBar, Modal } from 'react-native';
import { connect } from 'react-redux';
import {
  Container,
  Footer,
  Content,
  Col,
  H1,
  Row,
  P,
} from '@components/Global';
import Divider from '@components/Divider';
import Button from '@components/buttons/Button';
import VerifyModal from '@containers/VerifyModal';
import { useMutation } from '@apollo/react-hooks';
import { TRANSFER_POINT } from '@graphql/mutation';
import PanelTransparent from '@components/panels/PanelTransparent';

interface Props {
  route: any;
  accessToken: string;
  navigation: any;
}

const TransferStep2 = ({ route, accessToken, navigation }: Props) => {
  const [_isModal, _setModal] = useState(false);
  const [_phone] = useState(route.params.phone);
  const [_point] = useState(route.params.point);
  const [_receiver] = useState(route.params.receiver);

  const [transferPoint] = useMutation(TRANSFER_POINT, {
    onCompleted: (data: any) => {
      
      if (data?.transferPoint?.success) {
        navigation.navigate('TransferStep3');
      }
    },
  });

  const onTransferPoint = () => {
    _setModal(false);

    transferPoint({
      variables: {
        input: {
          phone: _phone,
          point: _point,
        },
      },
      context: { headers: { authorization: `Bearer ${accessToken}` } },
    });
  };

  const onComplete = () => {
    _setModal(true);
  };

  return (
    <>
      <Container>
        <Modal animationType="slide" transparent={true} visible={_isModal}>
          <VerifyModal
            onHideModal={() => {
              _setModal(false);
            }}
            onComplete={onTransferPoint}
          />
        </Modal>
        <StatusBar barStyle="dark-content" />
        <PanelTransparent title="ยืนยันการมอบแต้ม">
          <Row between>
            <Content>
              <P bold>ผู้รับแต้ม</P>
              <P>
                {_receiver.firstname} {_receiver.lastname}
              </P>
            </Content>
            <Content>
              <P bold right>
                จำนวนโอน
              </P>
              <P right>{_point} แต้ม</P>
            </Content>
          </Row>
          <Divider />
        </PanelTransparent>
        <Footer>
          <Button text="ยืนยันมอบแต้ม" onPress={() => onComplete()} />
        </Footer>
      </Container>
    </>
  );
};

const mapStateToProps = (state: any) => ({
  accessToken: state.auth.accessToken,
});

export default connect(mapStateToProps)(TransferStep2);
