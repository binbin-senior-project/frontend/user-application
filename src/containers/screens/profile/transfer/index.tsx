import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import AppbarBack from '@containers/appbars/AppbarBack';
import TransferStep1 from './TransferStep1';
import TransferStep2 from './TransferStep2';
import TransferStep3 from './TransferStep3';

const Stack = createStackNavigator();

const TransferStack = () => {
  return (
    <Stack.Navigator initialRouteName="TransferStep1" headerMode="screen">
      <Stack.Screen
        name="TransferStep1"
        component={TransferStep1}
        options={{
          header: props => (
            <AppbarBack title="มอบแต้ม" {...props} showPoint={false} />
          ),
        }}
      />
      <Stack.Screen
        name="TransferStep2"
        component={TransferStep2}
        options={{
          header: props => (
            <AppbarBack title="ยืนยันมอบแต้ม" {...props} showPoint={false} />
          ),
        }}
      />
      <Stack.Screen
        name="TransferStep3"
        component={TransferStep3}
        options={{
          header: () => null,
        }}
      />
    </Stack.Navigator>
  );
};

export default TransferStack;
