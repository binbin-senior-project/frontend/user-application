import React, { useState } from 'react';
import {
  View,
  TouchableOpacity,
  KeyboardAvoidingView,
  StatusBar,
} from 'react-native';
import validator from 'validator';
import { connect } from 'react-redux';
import auth from '@react-native-firebase/auth';
import Error from '@components/Error';
import Logo from '@components/Logo';
import Input from '@components/forms/Input';
import Button from '@components/buttons/Button';
import LoaderDefault from '@components/loaders/LoaderDefault';
import { setTokenAction } from '@actions/auth';
import { setProfileAction } from '@actions/user';
import { Container, Center, Row, H3, Col } from '@components/Global';

interface Props {
  setToken: Function;
  setProfile: Function;
  navigation: any;
}

const Login = ({ setToken, setProfile, navigation }: Props) => {
  const [_email, setEmail] = useState('');
  const [_password, setPassword] = useState('');
  const [_isLoading, _setLoading] = useState(false);
  const [_isError, _setError] = useState(false);
  const [_errorMsg, _setErrorMsg] = useState('');

  const onLogin = async () => {
    if (_email == '' || !validator.isEmail(_email)) {
      _setError(true);
      _setErrorMsg('อีเมล์ไม่ถูกต้อง');
      return;
    }

    if (
      _password == '' ||
      !_password.match(/(?=.*?[A-Za-z0-9])(?=.*?[^\w\s]).{7,}/)
    ) {
      _setError(true);
      _setErrorMsg('- รหัสผ่านควรยาว 7 ตัวอักษร\n- มีตัวอักษรใหญ่อย่างน้อย 1 ตัว\n- อักษรพิเศษอย่างน้อย 1 ตัว');
      return;
    }

    _setLoading(true);

    try {
      await auth().signInWithEmailAndPassword(_email, _password);
    } catch {
      _setLoading(false);
      _setError(true);
      _setErrorMsg("อีเมลหรือรหัสผ่านไม่ถูกต้อง")
      return;
    }
    
    const user = await auth().currentUser;
    const token = await user?.getIdToken();

    await setToken(token)
    _setLoading(false);
    setProfile();

    navigation.navigate('Main');
  };

  const goToRegister = () => {
    navigation.navigate('RegisterStep1');
  };

  return (
    <KeyboardAvoidingView behavior="padding">
      <StatusBar barStyle="dark-content" />
      <LoaderDefault isLoading={_isLoading} />
      <Error
        isError={_isError}
        message={_errorMsg}
        onPress={() => _setError(false)}
      />
      <Container>
        <Center>
          <Logo />
        </Center>
        <View style={{ height: 300 }}>
          <Input placeholder="อีเมล" value={_email} onChangeText={setEmail} />
          <Input
            type="password"
            placeholder="รหัสผ่าน"
            value={_password}
            onChangeText={setPassword}
          />
          <Col style={{ marginTop: 20 }}>
            <Button text="เข้าสู่ระบบ" onPress={onLogin} />
          </Col>
          <Row center>
            <H3>หากยังไม่ได้เป็นสมาชิก</H3>
            <TouchableOpacity onPress={goToRegister} style={{ marginLeft: 5 }}>
              <H3 green>สมัครสมาชิก</H3>
            </TouchableOpacity>
          </Row>
        </View>
      </Container>
    </KeyboardAvoidingView>
  );
};

const mapDispatchToProps = (dispatch: Function) => ({
  setToken: (token: string) =>
    dispatch(setTokenAction(token)),
    setProfile: () =>
    dispatch(setProfileAction()),
});

export default connect(
  null,
  mapDispatchToProps,
)(Login);
