import React, { useState } from 'react';
import { Image, KeyboardAvoidingView, StatusBar } from 'react-native';
import { connect } from 'react-redux';
import { useMutation } from '@apollo/react-hooks';
import Icon from 'react-native-vector-icons/Feather';
import Input from '@components/forms/Input';
import Button from '@components/buttons/Button';
import Error from '@components/Error';
import {
  Container,
  Content,
  H1,
  H3,
  Footer,
  Col,
  Row,
  P,
} from '@components/Global';
import { setPhoneAction } from 'src/actions/register';
import { REQUEST_OTP, IS_PHONE_EXIST } from '@graphql/mutation';
import Flag from '@images/flag.png';

interface Props {
  phone: string;
  setPhone: Function;
  navigation: any;
}

const RegisterStep1 = ({ phone, setPhone, navigation }: Props) => {
  const [_requestOTP] = useMutation(REQUEST_OTP);
  const [_phone, _setPhone] = useState(phone);
  const [_isError, _setError] = useState(false);
  const [_errorMsg, _setErrorMsg] = useState('');
  const [_isPhoneExist] = useMutation(IS_PHONE_EXIST, {
    onCompleted: (data: any) => {
      if (data?.isPhoneExist.success) {
        _setError(true);
        _setErrorMsg('เบอร์มือถือนี้ได้รับการลงทะเบียนแล้ว');
        return;
      }

      _requestOTP({ variables: { input: { phone: _phone } } });

      setPhone(phone);

      // Navigate to step 2
      navigation.navigate('RegisterStep2');
    },
  });

  const onNext = () => {
    if (_phone == null || _phone.length < 9 || _phone.length > 10) {
      _setError(true);
      _setErrorMsg('เบอร์มือถือไม่ถูกต้อง');
      return;
    }

    const phone = _phone.length == 9 ? '0' + _phone : _phone;
    setPhone(phone);

    _isPhoneExist({ variables: { input: { phone } } });
  };

  return (
    <KeyboardAvoidingView behavior="padding">
      <StatusBar barStyle="dark-content" />
      <Error
        message={_errorMsg}
        isError={_isError}
        onPress={() => _setError(false)}
      />
      <Container style={{ paddingTop: 60 }}>
        <Icon
          name="arrow-left"
          size={35}
          onPress={() => navigation.pop()}
          style={{ marginBottom: 10 }}
        />
        <Content>
          <H1>ระบุเบอร์มือถือ</H1>
          <H3 style={{ marginBottom: 20 }}>
            เพื่อยืนยันตัวตน OTP จะถูกส่งไปทางมือถือ
          </H3>
          <Row alignCenter>
            <Row alignCenter style={{ marginRight: 10 }}>
              <Image source={Flag} style={{ marginRight: 10 }} />
              <P>66</P>
            </Row>
            <Col style={{ flex: 1 }}>
              <Input
                type="number"
                placeholder="เบอร์มือถือ"
                value={_phone}
                onChangeText={text => _setPhone(text)}
              />
            </Col>
          </Row>
        </Content>
        <Footer>
          <Button showNextIcon text="ถัดไป" onPress={() => onNext()} />
        </Footer>
      </Container>
    </KeyboardAvoidingView>
  );
};

const mapStateToProps = (state: any) => ({
  phone: state.register.phone,
});

const mapDispatchToProps = (dispatch: Function) => ({
  setPhone: (phone: string) => dispatch(setPhoneAction(phone)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RegisterStep1);
