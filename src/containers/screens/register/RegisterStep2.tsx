import React, { useState } from 'react';
import { TouchableOpacity, KeyboardAvoidingView } from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, Footer, Row, H1, H3, P } from '@components/Global';
import Icon from 'react-native-vector-icons/Feather';
import Input from '@components/forms/Input';
import Button from '@components/buttons/Button';
import Error from '@components/Error';
import { useMutation } from '@apollo/react-hooks';
import { REQUEST_OTP, VERIFY_OTP } from '@graphql/mutation';
import LoaderDefault from '@components/loaders/LoaderDefault';

interface Props {
  phone: string;
  navigation: any;
}

const RegisterStep2 = ({ phone, navigation }: Props) => {
  const [_isLoading, _setLoading] = useState(false);
  const [_code, _setCode] = useState('');
  const [_isError, _setError] = useState(false);
  const [_requestOTP] = useMutation(REQUEST_OTP);
  const [_verifyOTP] = useMutation(VERIFY_OTP, {
    onCompleted: data => {
      _setLoading(false);

      if (!data?.verifyOTP?.success) {
        _setError(true);
        return;
      }

      navigation.navigate('RegisterStep3');
    },
  });

  const resendOTP = () => {
    // Resend API Here
    _requestOTP({
      variables: { input: { phone } },
    });
  };

  const verifyOTP = () => {
    _setLoading(true);
    _verifyOTP({ variables: { input: { phone, code: _code } } });
  };

  return (
    <KeyboardAvoidingView behavior="padding">
      <LoaderDefault isLoading={_isLoading} />
      <Error
        isError={_isError}
        message="รหัสยืนยันไม่ถูกต้อง"
        onPress={() => _setError(false)}
      />
      <Container style={{ paddingTop: 60 }}>
        <Icon
          name="arrow-left"
          size={35}
          onPress={() => navigation.pop()}
          style={{ marginBottom: 10 }}
        />
        <Content>
          <H1>ระบุรหัส OTP</H1>
          <H3 style={{ marginBottom: 20 }}>
            รหัสถูกส่งไปยังหมายเลข{' '}
            {phone.replace(/(\d{3})(\d{3})(\d{4})/, '$1 $2 $3')}
          </H3>
          <Input
            placeholder="หมายเลขยืนยัน"
            value={_code}
            onChangeText={(text: string) => _setCode(text)}
            type="number"
          />
          <Row center style={{ marginTop: 20 }}>
            <P>ยังไม่ได้รับรหัสยืนยัน?</P>
            <TouchableOpacity onPress={resendOTP} style={{ marginLeft: 5 }}>
              <H3 green>ส่งอีกครั้ง</H3>
            </TouchableOpacity>
          </Row>
        </Content>
        <Footer>
          <Button showNextIcon text="ถัดไป" onPress={() => verifyOTP()} />
        </Footer>
      </Container>
    </KeyboardAvoidingView>
  );
};

const mapStateToProps = (state: any) => ({
  phone: state.register.phone,
});

export default connect(mapStateToProps)(RegisterStep2);
