import React, { useState } from 'react';
import { KeyboardAvoidingView, StatusBar, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import validator from 'validator';
import Icon from 'react-native-vector-icons/Feather';
import Input from '@components/forms/Input';
import Button from '@components/buttons/Button';
import Error from '@components/Error';
import { Container, Footer, H1, H3, Content } from '@components/Global';
import {
  setFirstnameAction,
  setLastnameAction,
  setEmailAction,
  setPasswordAction,
} from 'src/actions/register';
import { useMutation } from '@apollo/react-hooks';
import { IS_EMAIL_EXIST } from '@graphql/mutation';

interface RegisterStep3Props {
  setFirstname: Function;
  setLastname: Function;
  setEmail: Function;
  setPassword: Function;
  navigation: any;
}

const RegisterStep3 = ({
  setFirstname,
  setLastname,
  setEmail,
  setPassword,
  navigation,
}: RegisterStep3Props) => {
  const [_firstname, _setFirstname] = useState('');
  const [_lastname, _setLastname] = useState('');
  const [_email, _setEmail] = useState('');
  const [_password, _setPassword] = useState('');
  const [_confirmPassword, _setConfirmPassword] = useState('');
  const [_isError, _setError] = useState(false);
  const [_errorMsg, _setErrorMsg] = useState('');
  const [_isEmailExist] = useMutation(IS_EMAIL_EXIST, {
    onCompleted: (data: any) => {
      if (data.isEmailExist.success) {
        _setError(true);
        _setErrorMsg('อีเมลนี้ได้รับการลงทะเบียนแล้ว');
        return;
      }

      setFirstname(_firstname);
      setLastname(_lastname);
      setEmail(_email);
      setPassword(_password);

      // Step to 4
      navigation.navigate('RegisterStep4');
    },
  });

  const onNext = () => {
    if (_firstname == '') {
      _setError(true);
      _setErrorMsg('โปรดระบุชื่อ');
      return;
    }

    if (_lastname == '') {
      _setError(true);
      _setErrorMsg('โปรดระบนามสกุล');
      return;
    }

    if (_email == '' || !validator.isEmail(_email)) {
      _setError(true);
      _setErrorMsg('อีเมล์ไม่ถูกต้อง');
      return;
    }

    if (!_password.match(/(?=.*?[A-Za-z0-9])(?=.*?[^\w\s]).{7,}/)) {
      _setError(true);
      _setErrorMsg(
        '- รหัสผ่านควรยาว 7 ตัวอักษร\n- มีตัวอักษรใหญ่อย่างน้อย 1 ตัว\n- อักษรพิเศษอย่างน้อย 1 ตัว',
      );
      return;
    }

    if (_password == '' || _password != _confirmPassword) {
      _setError(true);
      _setErrorMsg('รหัสผ่านไม่ตรงกัน');
      return;
    }

    _isEmailExist({
      variables: { input: { email: _email } },
    });
  };

  return (
    <KeyboardAvoidingView behavior="padding">
      <StatusBar barStyle="dark-content" />
      <Error
        isError={_isError}
        message={_errorMsg}
        onPress={() => _setError(false)}
      />
      <Container style={{ paddingTop: 60 }}>
        <Icon
          name="arrow-left"
          size={35}
          onPress={() => navigation.pop()}
          style={{ marginBottom: 10 }}
        />
        <ScrollView showsVerticalScrollIndicator={false}>
          <Content>
            <H1>สร้างบัญชี</H1>
            <H3 style={{ marginBottom: 20 }}>
              ระบุข้อมูลที่จำเป็นเพื่อเข้าใช้งาน
            </H3>
            <Input
              placeholder="ชื่อ"
              value={_firstname}
              onChangeText={(text: string) => _setFirstname(text)}
            />
            <Input
              placeholder="นามสกุล"
              value={_lastname}
              onChangeText={(text: string) => _setLastname(text)}
            />
            <Input
              placeholder="อีเมล"
              value={_email}
              onChangeText={(text: string) => _setEmail(text)}
            />
            <Input
              placeholder="รหัสผ่าน"
              value={_password}
              onChangeText={(text: string) => _setPassword(text)}
              type="password"
            />
            <Input
              placeholder="ยืนยันรหัสผ่าน"
              value={_confirmPassword}
              onChangeText={(text: string) => _setConfirmPassword(text)}
              type="password"
            />
          </Content>
        </ScrollView>
        <Footer>
          <Button showNextIcon text="ถัดไป" onPress={() => onNext()} />
        </Footer>
      </Container>
    </KeyboardAvoidingView>
  );
};

const mapDispatchToProps = (dispatch: Function) => ({
  setFirstname: (value: string) => dispatch(setFirstnameAction(value)),
  setLastname: (value: string) => dispatch(setLastnameAction(value)),
  setEmail: (value: string) => dispatch(setEmailAction(value)),
  setPassword: (value: string) => dispatch(setPasswordAction(value)),
});

export default connect(
  null,
  mapDispatchToProps,
)(RegisterStep3);
