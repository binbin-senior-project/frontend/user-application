import React, { useState } from 'react';
import { StatusBar } from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Feather';
import Passcode from '@components/Passcode';
import { Container, Content, H1, H3 } from '@components/Global';
import { setPasscodeAction } from 'src/actions/register';

interface Props {
  setPasscode: Function;
  navigation: any;
}

const RegisterStep4 = ({ setPasscode, navigation }: Props) => {
  const [_passcode, _setPasscode] = useState([]);

  const onNext = () => {
    setPasscode(_passcode.join(''));
    navigation.push('RegisterStep5');
  };

  return (
    <Container style={{ paddingTop: 60 }}>
      <StatusBar barStyle="dark-content" />
      <Icon
        name="arrow-left"
        size={35}
        onPress={() => navigation.pop()}
        style={{ marginBottom: 10 }}
      />
      <Content>
        <H1>ตั้งค่ารหัส PIN</H1>
        <H3 stle={{ marginBottom: 20 }}>เพื่อป้องกันการเข้าใช้งานจากผู้อื่น</H3>
        <Passcode
          passcode={_passcode}
          onPress={(passcode: any) => _setPasscode(passcode)}
          onComplete={onNext}
        />
      </Content>
    </Container>
  );
};

const mapDispatchToProps = (dispatch: Function) => ({
  setPasscode: (value: string) => dispatch(setPasscodeAction(value)),
});

export default connect(
  null,
  mapDispatchToProps,
)(RegisterStep4);
