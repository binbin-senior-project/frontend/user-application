import React, { useState } from 'react';
import { StatusBar } from 'react-native';
import { connect } from 'react-redux';
import { Container, Content, H1, H3 } from '@components/Global';
import Icon from 'react-native-vector-icons/Feather';
import Passcode from '@components/Passcode';
import LoaderDefault from '@components/loaders/LoaderDefault';
import { registerAccountAction } from '@actions/register';

interface Props {
  passcode: string;
  registerAccount: Function;
  navigation: any;
}

const RegisterStep5 = ({ passcode, registerAccount, navigation }: Props) => {
  const [_passcode, _setPasscode] = useState([]);
  const [isLoading, setLoading] = useState(false);

  const onNext = async () => {
    setLoading(true);

    if (passcode != _passcode.join('')) {
      setLoading(false);
      _setPasscode([]);
      return;
    }

    await registerAccount();

    setLoading(false);
    navigation.navigate('RegisterStep6');
  };

  return (
    <Container style={{ paddingTop: 60 }}>
      <StatusBar barStyle="dark-content" />
      <LoaderDefault isLoading={isLoading} />
      <Icon
        name="arrow-left"
        size={35}
        onPress={() => navigation.pop()}
        style={{ marginBottom: 10 }}
      />
      <Content>
        <H1>ยืนยันรหัส PIN</H1>
        <H3 stle={{ marginBottom: 20 }}>เพื่อป้องกันการเข้าใช้งานจากผู้อื่น</H3>
        <Passcode
          passcode={_passcode}
          onPress={(passcode: any) => _setPasscode(passcode)}
          onComplete={onNext}
        />
      </Content>
    </Container>
  );
};

const mapStateToProps = (state: any) => ({
  passcode: state.register.passcode,
});

const mapDispatchToProps = (dispatch: Function) => ({
  registerAccount: () => dispatch(registerAccountAction()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RegisterStep5);
