import React from 'react';
import { View, Image } from 'react-native';
import { Container, Footer, Content, H1, P, Col } from '@components/Global';
import Swiper from 'react-native-swiper';
import Button from '@components/buttons/Button';
import colors from '@utils/colors';

import welcome1 from '@images/welcome/welcome1.png';
import welcome2 from '@images/welcome/welcome2.png';
import welcome3 from '@images/welcome/welcome3.png';

interface Props {
  navigation: any;
}

const RegisterStep6 = ({ navigation }: Props) => {
  const welcomes = [
    {
      image: welcome1,
      title: 'ทิ้งขวดเพื่อสะสมแต้ม',
      description: 'ด้วยการสแกน QR Code บนถัง Binbin',
    },
    {
      image: welcome2,
      title: 'ใช้แต้มสะสมแลกคูปอง',
      description: 'สามารถแลกคูปอง บริจาค หรือแลกเป็นเงินได้',
    },
    {
      image: welcome3,
      title: 'สร้างวินัยลดขยะให้โลกของเรา',
      description: 'สร้างสังคมและสภาพแวดล้อมที่ดีขึ้นกว่าเดิม',
    },
  ];

  return (
    <Container
      disablePadding
      style={{
        paddingTop: 60,
      }}>
      <Content>
        <H1 style={{ textAlign: 'center' }}>สมัครสมาชิกเรียบร้อย</H1>
        <P style={{ textAlign: 'center', marginBottom: 20 }}>
          เลื่อนเพื่อดูคำแนะนำ
        </P>
        <Swiper
          style={{ height: '100%' }}
          dotColor={colors.iconColor}
          activeDotColor={colors.secondaryColor}
          showsPagination
          autoplay
          autoplayTimeout={5}
          bounces>
          {welcomes.map((welcome) => (
            <Col
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                padding: 20,
                height: '100%',
              }}>
              <Image
                style={{
                  height: 250,
                  marginBottom: 80,
                }}
                source={welcome.image}
                resizeMode="contain"
              />
              <H1>{welcome.title}</H1>
              <P>{welcome.description}</P>
            </Col>
          ))}
        </Swiper>
      </Content>
      <Footer
        style={{
          padding: 20,
          height: 140,
        }}>
        <Button
          isWhite
          text="เริ่มต้นใช้งาน"
          onPress={() => navigation.navigate('Main')}
        />
      </Footer>
    </Container>
  );
};

export default RegisterStep6;
