import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Login from './Login';
import RegisterStep1 from './RegisterStep1';
import RegisterStep2 from './RegisterStep2';
import RegisterStep3 from './RegisterStep3';
import RegisterStep4 from './RegisterStep4';
import RegisterStep5 from './RegisterStep5';
import RegisterStep6 from './RegisterStep6';

const Stack = createStackNavigator();

const RegisterStack = () => {
  return (
    <Stack.Navigator initialRouteName="Login" headerMode="none">
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="RegisterStep1" component={RegisterStep1} />
      <Stack.Screen name="RegisterStep2" component={RegisterStep2} />
      <Stack.Screen name="RegisterStep3" component={RegisterStep3} />
      <Stack.Screen name="RegisterStep4" component={RegisterStep4} />
      <Stack.Screen name="RegisterStep5" component={RegisterStep5} />
      <Stack.Screen
        name="RegisterStep6"
        component={RegisterStep6}
        options={{ animationEnabled: false }}
      />
    </Stack.Navigator>
  );
};

export default RegisterStack;
