import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Dimensions } from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import BleManager from 'react-native-ble-manager';
import LoaderDefault from '@components/loaders/LoaderDefault';
import { Container, H3 } from '@components/Global';

const styles = StyleSheet.create({
  maskerCol: {
    position: 'absolute',
    top: 0,
    zIndex: 2,
    width: '100%',
    height: '100%',
    justifyContent: 'space-between',
  },
  maskerRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1,
  },
  maskerBox: {
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    flex: 1,
  },
  maskerCamera: {
    backgroundColor: 'transparent',
    flex: 4,
    borderWidth: 2,
    borderColor: '#ffffff',
  },
});

interface Props {
  navigation: any;
}

const ScanStep1 = ({ navigation }: Props) => {
  const [_isLoading, _setLoading] = useState(false);

  const onSuccess = async (e: any) => {
    _setLoading(true);

    await BleManager.start();

    _setLoading(false);

    navigation.push('ScanStep2', {
      qrcode: e.data,
    });
  };

  return (
    <Container disablePadding>
      <LoaderDefault isLoading={_isLoading} />
      <View style={styles.maskerCol}>
        <View style={styles.maskerBox} />
        <View style={styles.maskerRow}>
          <View style={styles.maskerBox} />
          <View style={styles.maskerCamera} />
          <View style={styles.maskerBox} />
        </View>
        <View style={styles.maskerBox} />
      </View>

      <QRCodeScanner
        onRead={onSuccess}
        showMarker
        reactivate
        reactivateTimeout={5000}
        cameraStyle={{ height: Dimensions.get('window').height }}
        markerStyle={{ borderWidth: 0 }}
        topViewStyle={{ height: 0, flex: 0 }}
      />

      <View
        style={{
          position: 'absolute',
          bottom: 60,
          zIndex: 3,
          width: '100%',
        }}>
        <H3 white center>
          สแกน QR Code ได้ที่ถัง Binbin
        </H3>
        <H3 white center>
          เพื่อรับแต้มจากขวด
        </H3>
      </View>
    </Container>
  );
};

export default ScanStep1;
