import React, { useState, useEffect } from 'react';
import {
  View,
  NativeModules,
  NativeEventEmitter,
  Vibration,
} from 'react-native';
import { connect } from 'react-redux';
import { useMutation, useLazyQuery } from '@apollo/react-hooks';
import {
  Container,
  H1,
  H3,
  P,
  Content,
  Footer,
} from '@components/Global';
import BleManager from 'react-native-ble-manager';
import BottleTable from '@components/BottleTable';
import Logo from '@components/Logo';
import Button from '@components/buttons/Button';
import LoaderDefault from '@components/loaders/LoaderDefault';
import { GET_TRASH_BYCODE } from '@graphql/query';
import { COLLECT_POINT } from '@graphql/mutation';

const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

interface Props {
  accessToken: string;
  route: any;
  navigation: any;
}

const ScanStep2 = ({ accessToken, route, navigation }: Props) => {

  const [bottles, setBottles] = useState([]);
  const [peripheral, setPeripheral] = useState(null);
  const [isConnectBLE, setConnectBLE] = useState(false);
  const [isConfirm, setConfirm] = useState(false);
  const [_getTrashByCode] = useLazyQuery(GET_TRASH_BYCODE, {
    fetchPolicy: 'no-cache',
    onCompleted: (data: any) => {

      if (data?.getTrashByCode == null)  return;

      Vibration.vibrate();

      // Build object
      const bottle = {
        code: data.getTrashByCode.code,
        point: data.getTrashByCode.salePoint,
      };

      setBottles(bottles => ([...bottles, bottle]));
    }
  });

  const onCollectComplete = async (data: any) => {
    if (data?.collectPoint?.success) {
      setConfirm(false)

      const ppID = peripheral?.id;
      const ppSID = peripheral?.advertising.serviceUUIDs[0];
      const ppCID = '6E400003-B5A3-F393-E0A9-E50E24DCCA9E';

      await BleManager.stopNotification(ppID, ppSID, ppCID)
      
      navigation.navigate('ScanStep3');
    }
  }

  const [collectPoint] = useMutation(COLLECT_POINT, {
    onCompleted: onCollectComplete,
  });

  const onDidUpdateValueForCharacteristic = async ({ value }: { value: any }) => {
    const prevent = [10, 116, 78, 32];

    if (prevent.includes(value[0])) return;

    let code = value.map((ascii: number) => String.fromCharCode(ascii)).join('')

    // Get current point from backend
    _getTrashByCode({
      variables: { code },
      context: { headers: { authorization: `Bearer ${accessToken}` } }
    });
  };

  const onDiscoverPeripheral = async (peripheral: any) => {
    const ppID = peripheral.id;
    const ppSID = peripheral.advertising.serviceUUIDs[0];
    const ppCID = '6E400003-B5A3-F393-E0A9-E50E24DCCA9E';

    await BleManager.connect(ppID)
    await BleManager.retrieveServices(ppID)
    await BleManager.startNotification(ppID, ppSID, ppCID)

    setPeripheral(peripheral);
  }

  const onConfirm = async () => {
    setConfirm(true);

    collectPoint({
      variables: {
        input: {
          trashCodes: bottles.map(bottle => bottle.code)
        }
      },
      context: {
        headers: {
          authorization: `Bearer ${accessToken}`
        }
      }
    })
  }

  const startBluethooth = async () => {
    setConnectBLE(false);

    bleManagerEmitter.addListener(
      'BleManagerDiscoverPeripheral',
      onDiscoverPeripheral
    );
    
    bleManagerEmitter.addListener(
      'BleManagerDidUpdateValueForCharacteristic',
      onDidUpdateValueForCharacteristic
    );

    BleManager.scan([route.params.qrcode], 5)
  };

  useEffect(() => {
    if (!isConnectBLE) {
      startBluethooth();
      setConnectBLE(true);
    }  
  }, [navigation]);

  return (
    <Container>
      <LoaderDefault isLoading={isConfirm} />
      <Content>
        <H1>Binbin พร้อมแล้ว</H1>
        <H3 style={{ marginBottom: 30 }}>
          จำนวนพอยต์ขึ้นอยู่กับคุณภาพของขวด
        </H3>
        {!!bottles.length ? (
          <BottleTable bottles={bottles} />
        ) : (
          <View style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
            <Logo />
            <P style={{ marginTop: 20 }}>
              หยิบขวดขึ้นมาทิ้งในถัง Binbin
            </P>
          </View>
        )}
      </Content>
      {!!bottles.length && (
        <Footer>
          <Button
            showNextIcon
            text="ยืนยันรับแต้ม"
            onPress={onConfirm}
          />
        </Footer>
      )}
    </Container>
  );
};

const mapStateToProps = (state: any) => ({
  accessToken: state.auth.accessToken,
});

export default connect(mapStateToProps)(ScanStep2);
