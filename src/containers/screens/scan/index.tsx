import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import AppbarDefault from '@containers/appbars/AppbarDefault';
import AppbarBack from '@containers/appbars/AppbarBack';
import ScanStep1 from '@containers/screens/scan/ScanStep1';
import ScanStep2 from '@containers/screens/scan/ScanStep2';
import ScanStep3 from '@containers/screens/scan/ScanStep3';

const Stack = createStackNavigator();

const ScanStack = () => {
  return (
    <Stack.Navigator initialRouteName="StoreScreen" headerMode="screen">
      <Stack.Screen
        name="ScanStep1"
        component={ScanStep1}
        options={{
          header: props => <AppbarDefault title="คูปอง" {...props} />,
        }}
      />
      <Stack.Screen
        name="ScanStep2"
        component={ScanStep2}
        options={{
          header: props => <AppbarBack title="ร้านค้า" {...props} />,
        }}
      />
      <Stack.Screen
        name="ScanStep3"
        component={ScanStep3}
        options={{
          header: () => null,
        }}
      />
    </Stack.Navigator>
  );
};

export default ScanStack;
