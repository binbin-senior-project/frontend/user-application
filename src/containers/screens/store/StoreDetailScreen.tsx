import React, { useState, useEffect } from 'react';
import {
  View,
  Image,
  ScrollView,
  ActivityIndicator,
  Linking,
  Platform,
} from 'react-native';
import { connect } from 'react-redux';
import moment from 'moment';
import { Container, H1, H3, P, Row, Col, Span } from '@components/Global';
import colors from '@utils/colors';
import CouponList from '@components/CouponList';
import { useQuery, useLazyQuery } from '@apollo/react-hooks';
import { GET_STORE, GET_COUPON_BY_STORE } from '@graphql/query';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

interface Props {
  route: any;
  accessToken: string;
  navigation: any;
}

const StoreDetailScreen = ({ route, accessToken, navigation }: Props) => {
  const [_store, _setStore] = useState(null);
  const [_coupons, _setCoupons] = useState([]);
  const [_offset, _setOffset] = useState(0);
  const [_isLoading, _setLoading] = useState(true);
 
  const renderNewCoupon = (couponData: any) => {
    if (couponData == null) return;

    const newCoupons = couponData.map((item: any) => {
      const d = Date.parse(item.expire.replace(/-/gi, '/').split('.')[0]);
      const expire = moment(d).locale('th');

      return {
        id: item.id,
        photo: item.photo,
        name: item.name,
        point: item.point,
        expire: expire.add(543, 'y').format('ll HH:mm'),
        store: item.store,
      };
    });

    _setCoupons([..._coupons, ...newCoupons]);
  };

  const [_getStore] = useLazyQuery(GET_STORE, {
    onCompleted: (data: any) => {
      _setLoading(false);

      if (data?.getStore == null) return; 

      _setStore(data.getStore);
      renderNewCoupon(data.getStore.coupons);
    },
  });

  const onEndReached = () => {
    _setOffset(_offset + 6);
  };

  const openMap = (storeName: string, lat: string, lng: string) => {
    const scheme = Platform.select({
      ios: 'maps:0,0?q=',
      android: 'geo:0,0?q=',
    });
    const latLng = `${lat},${lng}`;
    const label = storeName;
    const url = Platform.select({
      ios: `${scheme}${label}@${latLng}`,
      android: `${scheme}${latLng}(${label})`,
    });

    Linking.openURL(url);
  };

  useEffect(() => {
    _setLoading(true);

    _getStore({
      variables: { storeID: route.params.storeID },
      context: { headers: { authorization: `Bearer ${accessToken}` } },
    })
  }, [navigation])

  if (_isLoading) {
    return (
      <Container>
        <ActivityIndicator color={colors.primaryColor} />
      </Container>
    );
  }

  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <Container disablePadding>
        <View
          style={{
            backgroundColor: colors.whiteColor,
            paddingTop: 15,
            paddingBottom: 25,
            paddingHorizontal: 20,
            borderBottomWidth: 1,
            borderBottomColor: colors.grayBorder,
          }}>
          <Row alignCenter>
            <Col style={{ paddingRight: 20 }}>
              <Image
                source={{ uri: _store.logo }}
                style={{
                  width: 60,
                  height: 60,
                  borderRadius: 60,
                }}
              />
            </Col>
            <Col style={{ flex: 1 }}>
              <H1>{_store.name}</H1>
              <Row alignCenter>
                <P>ประเภทร้าน{_store.category.name}</P>
                <P style={{ marginHorizontal: 10, color: colors.grayBorder }}>
                  |
                </P>
                <TouchableWithoutFeedback
                  onPress={() => Linking.openURL(`tel:${_store.phone}`)}>
                  <Span>โทรหาร้าน</Span>
                </TouchableWithoutFeedback>
                <P style={{ marginHorizontal: 10, color: colors.grayBorder }}>
                  |
                </P>
                <TouchableWithoutFeedback
                  onPress={() => openMap(_store.name, _store.latitude, _store.longitude)}>
                  <Span>ดูแผนที่</Span>
                </TouchableWithoutFeedback>
              </Row>
            </Col>
          </Row>
        </View>
        <CouponList
          isLoading={_isLoading}
          coupons={_coupons}
          onEndReached={onEndReached}
        />
      </Container>
    </ScrollView>
  );
};

const mapStateToProps = (state: any) => ({
  accessToken: state.auth.accessToken,
});

export default connect(mapStateToProps)(StoreDetailScreen);
