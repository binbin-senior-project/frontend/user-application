import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { useQuery, useLazyQuery } from '@apollo/react-hooks';
import StoreList from '@components/StoreList';
import Tab from '@components/Tab';
import {
  GET_STORES,
  GET_STORE_CATEGORIES,
  GET_STORES_BY_CATEGORY,
} from '@graphql/query';

interface Props {
  accessToken: string;
  navigation: any;
}

const StoreScreen = ({ accessToken, navigation }: Props) => {
  const [storeCategories, setStoreCategories] = useState([
    { id: 'all', title: 'ทั้งหมด' },
  ]);
  const [_currentCategory, _setCurrentCategory] = useState('all');
  const [_stores, _setStores] = useState([]);
  const [_offset, _setOffset] = useState(0);
  const [_isLoading, _setLoading] = useState(true);

  const onTabPress = (categoryID: string) => {
    if (_currentCategory == categoryID) return;

    _setCurrentCategory(categoryID);
    _setOffset(0);
  };

  const renderNewStore = (storeData: any) => {
    if (storeData == null) return;

    const stores = storeData.map((item: any) => {
      return {
        id: item.id,
        logo: item.logo,
        name: item.name,
        tagline: item.tagline,
      };
    });

    return stores;
  };

  const onEndReached = () => {
    _setOffset(_offset + 6);
  };

  const onRefresh = () => {

    if (_currentCategory == 'all') {
      _getStores({
        variables: { offset: 0, limit: 12 },
        context: { headers: { authorization: `Bearer ${accessToken}` } },
      })
      return;
    }

    _getStoresByCategory({
      variables: { categoryID: _currentCategory, offset: 0, limit: 12 },
      context: { headers: { authorization: `Bearer ${accessToken}` } },
    });
  }

  // Get Categories
  useQuery(GET_STORE_CATEGORIES, {
    fetchPolicy: 'no-cache',
    errorPolicy: 'ignore',
    variables: { offset: 0, limit: 100 },
    context: { headers: { authorization: `Bearer ${accessToken}` } },
    onCompleted: (data: any) => {
      if (data?.getStoreCategories == null) return;
      
      const newStoreCategories = data?.getStoreCategories.map((item: any) => {
        return {
          id: item.id,
          title: item.name,
        };
      });

      setStoreCategories([...storeCategories, ...newStoreCategories]);
    },
  });

  // Get Coupons
  const [_getStores] = useLazyQuery(GET_STORES, {
    fetchPolicy: 'no-cache',
    errorPolicy: 'ignore',
    onCompleted: (data: any) => {
      _setLoading(false);
      _setOffset(0);

      if (data?.getStores == null) {
        _setStores([]);
        return;
      }

      const stores = renderNewStore(data.getStores);
      _setStores(stores);
    },
  });

  // Get Coupons
  const [_loadMoreStores] = useLazyQuery(GET_STORES, {
    fetchPolicy: 'no-cache',
    errorPolicy: 'ignore',
    onCompleted: (data: any) => {
      _setLoading(false);

      if (data?.getStores == null) return;

      const newStores = renderNewStore(data.getStores);
      _setStores([..._stores, ...newStores]);
    },
  });

  // Get Coupons By Category
  const [_getStoresByCategory] = useLazyQuery(GET_STORES_BY_CATEGORY, {
    fetchPolicy: 'no-cache',
    errorPolicy: 'ignore',
    onCompleted: (data: any) => {
      _setLoading(false);
      _setOffset(0);

      if (data.getStoresByCategory == null) {
        _setStores([]);
        return;
      }

      const stores = renderNewStore(data?.getStoresByCategory);
      _setStores(stores);
    },
  });

  // Get stores by category
  const [_loadMoreStoresByCategory] = useLazyQuery(GET_STORES_BY_CATEGORY, {
    fetchPolicy: 'no-cache',
    errorPolicy: 'ignore',
    onCompleted: (data: any) => {
      _setLoading(false);

      if (data.getStoresByCategory == null) return;

      const newStores = renderNewStore(data?.getStoresByCategory);
      _setStores([..._stores, ...newStores]);
    },
  });

  useEffect(() => {
    _setLoading(true);

    const unsubscribe = navigation.addListener('focus', () => {
      _getStores({
       variables: { offset: 0, limit: 12 },
       context: { headers: { authorization: `Bearer ${accessToken}` } },
      })
    });

    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    if (_offset == 0) return;

    if (_currentCategory == 'all') {
      _loadMoreStores({
        variables: { offset: _offset, limit: 12 },
        context: { headers: { authorization: `Bearer ${accessToken}` } },
      })
      return;
    }

    _loadMoreStoresByCategory({
      variables: { categoryID: _currentCategory, offset: _offset, limit: 12 },
      context: { headers: { authorization: `Bearer ${accessToken}` } },
    });
  }, [_offset])

  useEffect(() => {

    if (_currentCategory == 'all') {
      _getStores({
        variables: { offset: 0, limit: 12 },
        context: { headers: { authorization: `Bearer ${accessToken}` } },
      });
      return;
    }

    _getStoresByCategory({
      variables: { categoryID: _currentCategory, offset: 0, limit: 12 },
      context: { headers: { authorization: `Bearer ${accessToken}` } },
    });
  }, [_currentCategory])

  useEffect(() => {
    const unsubscribe = navigation.addListener('blur', () => {
     _setOffset(0);
     _setCurrentCategory('all');
    });

    return unsubscribe;
  }, [navigation]);

  return (
    <>
      <Tab items={storeCategories} onPress={onTabPress} onPressSearch={() => navigation.navigate('StoreSearch')} />
      <StoreList
        isLoading={_isLoading}
        stores={_stores}
        onEndReached={onEndReached}
        onRefresh={onRefresh}
        scrollEnabled
      />
    </>
  );
};

const mapStateToProps = (state: any) => ({
  accessToken: state.auth.accessToken,
})

export default connect(mapStateToProps)(StoreScreen);
