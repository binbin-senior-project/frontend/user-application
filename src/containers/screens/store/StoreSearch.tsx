import React, { useEffect, useState } from 'react';
import { ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { useLazyQuery } from '@apollo/react-hooks';
import StoreList from '@components/StoreList';
import { GET_STORES_BY_SEARCH } from '@graphql/query';
import { H3, Row } from '@components/Global';
import colors from '@utils/colors';

interface Props {
  accessToken: string;
  keyword: string;
}

const StoreSearch = ({ accessToken, keyword }: Props) => {
  const [stores, setStores] = useState([]);
  const [offset, setOffset] = useState(0);
  const [isLoading, setLoading] = useState(false);

  const renderNewStore = (storeData: any) => {
    if (storeData == null) return;

    const newStores = storeData.map((item: any) => {
      return {
        id: item.id,
        logo: item.logo,
        name: item.name,
        tagline: item.tagline,
      };
    });

    setStores([...stores, ...newStores]);
  };

  const onEndReached = () => {
    setOffset(offset + 6);
  };

  const [getStoresByearch] = useLazyQuery(GET_STORES_BY_SEARCH, {
    fetchPolicy: 'no-cache',
    onCompleted: (data: any) => {
      setLoading(false);
      renderNewStore(data.getStoresBySearch);
    },
  });

  useEffect(() => {
    setStores([]);
    setOffset(0);
    setLoading(true);
  }, [keyword]);

  useEffect(() => {
    getStoresByearch({
      variables: { keyword, offset, limit: 6 },
      context: { headers: { authorization: `Bearer ${accessToken}` } },
    });
  }, [keyword, offset]);

  return (
    <ScrollView
      contentContainerStyle={{
        height: '100%',
      }}>
      {keyword != '' && (
        <>
          <Row
            style={{
              backgroundColor: colors.whiteColor,
              paddingHorizontal: 20,
              paddingVertical: 20,
              borderBottomWidth: 1,
              borderBottomColor: colors.inputBackgroundColor,
            }}>
            <H3 bold>{`ผลการค้นหา: ${keyword}`}</H3>
          </Row>
          <StoreList
            isLoading={isLoading}
            stores={stores}
            onEndReached={onEndReached}
            scrollEnabled={false}
          />
        </>
      )}
    </ScrollView>
  );
};

const mapStateToProps = (state: any) => ({
  accessToken: state.auth.accessToken,
  keyword: state.search.store,
});

export default connect(mapStateToProps)(StoreSearch);
