import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import AppbarDefault from '@containers/appbars/AppbarDefault';
import AppbarBack from '@containers/appbars/AppbarBack';
import AppbarStoreSearch from '@containers/appbars/AppbarStoreSearch';
import StoreScreen from '@containers/screens/store/StoreScreen';
import CouponDetailScreen from '@containers/screens/coupon/CouponDetailScreen';
import StoreDetailScreen from '@containers/screens/store/StoreDetailScreen';
import StoreSearch from '@containers/screens/store/StoreSearch';

const Stack = createStackNavigator();

const StoreStack = () => {
  return (
    <Stack.Navigator initialRouteName="StoreScreen" headerMode="screen">
      <Stack.Screen
        name="StoreScreen"
        component={StoreScreen}
        options={{
          header: props => <AppbarDefault title="ร้านค้า" {...props} />,
        }}
      />
      <Stack.Screen
        name="StoreSearch"
        component={StoreSearch}
        options={{
          header: props => <AppbarStoreSearch title="ร้านค้า" {...props} />,
          animationEnabled: false,
        }}
      />
      <Stack.Screen
        name="CouponDetailScreen"
        component={CouponDetailScreen}
        options={{
          header: props => <AppbarBack title="คูปอง" {...props} />,
        }}
      />
      <Stack.Screen
        name="StoreDetailScreen"
        component={StoreDetailScreen}
        options={{
          header: props => <AppbarBack title="ร้านค้า" {...props} />,
        }}
      />
    </Stack.Navigator>
  );
};

export default StoreStack;
