import React, { useState, useEffect } from 'react';
import {
  ScrollView,
  RefreshControl,
  Image,
  ActivityIndicator,
  FlatList,
} from 'react-native';
import { connect } from 'react-redux';
import { useLazyQuery } from '@apollo/react-hooks';
import { Container } from '@components/Global';
import Panel from '@components/panels/Panel';
import NoData from '@components/NoData';
import TransactionListItem from '@components/TransactionListItem';
import { GET_TRANSACTIONS } from '@graphql/query';
import colors from '@utils/colors';

interface Props {
  accessToken: string;
  navigation: any;
}

const TransactionScreen = ({ accessToken, navigation }: Props) => {
  const [_transactions, _setTransactions] = useState([]);
  const [_offset, _setOffset] = useState(0);
  const [_isLoading, _setLoading] = useState(false);

  const [_getTransactions] = useLazyQuery(GET_TRANSACTIONS, {
    fetchPolicy: 'no-cache',
    onCompleted: (data: any) => {
      _setLoading(false);

      if (data.getTransactions == null) return;

      _setTransactions(data.getTransactions);
      _setOffset(0);
    },
  });

  const [_loadMoreTransactions] = useLazyQuery(GET_TRANSACTIONS, {
    fetchPolicy: 'no-cache',
    onCompleted: (data: any) => {
      _setLoading(false);

      if (data.getTransactions == null) return;

      _setTransactions([..._transactions, ...data.getTransactions]);
    },
  });

  const onRefresh = () => {
    _setLoading(true);

    _getTransactions({
      variables: { offset: _offset, limit: 100 },
      context: { headers: { authorization: `Bearer ${accessToken}` } },
    });
  };

  const onEndReached = () => {
    _setOffset(_offset + 6);
  };

  const goToReceiptPage = (receipt: any) => {
    switch (receipt.type) {
      case 'collect':
        navigation.navigate('PointReceiptScreen', {
          transactionID: receipt.id,
        });
        break;
      case 'transfer':
        navigation.navigate('TransferReceiptScreen', {
          transactionID: receipt.id,
        });
        break;
      case 'redeem':
        navigation.navigate('RedeemReceiptScreen', {
          transactionID: receipt.id,
        });
        break;
    }
  };

  useEffect(() => {
    _setLoading(true);

    const unsubscribe = navigation.addListener('focus', () => {
      _getTransactions({
        variables: { offset: _offset, limit: 100 },
        context: { headers: { authorization: `Bearer ${accessToken}` } },
      });
    });

    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    if (_offset == 0) return;

    _loadMoreTransactions({
      variables: { offset: _offset, limit: 6 },
      context: { headers: { authorization: `Bearer ${accessToken}` } },
    });
  }, [_offset]);

  return (
    <>
      {!_isLoading && _transactions.length == 0 && (
        <NoData title="ไม่มีการทำรายการ" />
      )}
      <FlatList
        data={_transactions}
        refreshControl={
          <RefreshControl
            refreshing={_isLoading}
            onRefresh={() => onRefresh()}
            tintColor={colors.primaryColor}
          />
        }
        onEndReached={() => onEndReached()}
        onEndReachedThreshold={0.8}
        numColumns={1}
        renderItem={({ item }: { item: any }) => {
          return (
            <TransactionListItem
              key={item.id}
              id={item.id}
              point={item.type == 'collect' ? item.point : -item.point}
              description={item.description}
              createdAt={item.createdAt}
              onPress={() => goToReceiptPage(item)}
            />
          );
        }}
        keyExtractor={item => item.id}
        contentContainerStyle={{
          marginTop: 10,
          paddingBottom: 10,
        }}
      />
    </>
  );
};

const mapStateToProps = (state: any) => ({
  accessToken: state.auth.accessToken,
});

export default connect(mapStateToProps)(TransactionScreen);
