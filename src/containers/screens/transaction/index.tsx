import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import AppbarDefault from '@containers/appbars/AppbarDefault';
import AppbarBack from '@containers/appbars/AppbarBack';
import TransactionScreen from '@containers/screens/transaction/TransactionScreen';
import PointReceiptScreen from '@containers/screens/transaction/PointReceiptScreen';
import TransferReceiptScreen from '@containers/screens/transaction/TransferReceiptScreen';
import RedeemReceiptScreen from '@containers/screens/transaction/RedeemReceiptScreen';

const Stack = createStackNavigator();

const TransactionStack = () => {
  return (
    <Stack.Navigator initialRouteName="TransactionScreen" headerMode="screen">
      <Stack.Screen
        name="TransactionScreen"
        component={TransactionScreen}
        options={{
          header: props => <AppbarDefault title="รายการ" {...props} />,
        }}
      />
      <Stack.Screen
        name="PointReceiptScreen"
        component={PointReceiptScreen}
        options={{
          header: props => (
            <AppbarBack title="รับแต้ม" {...props} showPoint={false} />
          ),
        }}
      />
      <Stack.Screen
        name="TransferReceiptScreen"
        component={TransferReceiptScreen}
        options={{
          header: props => (
            <AppbarBack title="มอบแต้ม" {...props} showPoint={false} />
          ),
        }}
      />
      <Stack.Screen
        name="RedeemReceiptScreen"
        component={RedeemReceiptScreen}
        options={{
          header: props => (
            <AppbarBack title="ใช้คูปอง" {...props} showPoint={false} />
          ),
        }}
      />
    </Stack.Navigator>
  );
};

export default TransactionStack;
