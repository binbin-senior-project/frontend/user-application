import React, { useState } from 'react';
import { FlatList } from 'react-native';
import { useQuery } from '@apollo/react-hooks';
import CouponBox from '@components/CouponItem';
import { GET_COUPONS } from '@graphql/query';

interface Props {}

const StoreCoupons = () => {
  const [coupons, setCoupon] = useState([]);
  const [offset, setOffset] = useState(0);

  const onLoadComplete = (data: any) => {
    if (data.getCoupons) {
      setCoupon([...coupons, ...data.getCoupons]);
    }
  };

  const { refetch } = useQuery(GET_COUPONS, {
    variables: {
      offset: offset,
      limit: 6,
    },
    onCompleted: onLoadComplete,
  });

  const onEndReached = () => {
    setOffset(offset + 6);
    refetch();
  };

  return (
    <FlatList
      scrollEnabled={false}
      data={coupons}
      onEndReached={onEndReached}
      onEndReachedThreshold={0.4}
      numColumns={2}
      renderItem={({ item }: { item: any }) => {
        return (
          <CouponBox
            key={item.id}
            id={item.id}
            logo={item.logo}
            image={item.photo}
            name={item.name}
            point={item.point}
            expire={item.expire}
          />
        );
      }}
      keyExtractor={item => item.id}
      contentContainerStyle={{ padding: 20 }}
      columnWrapperStyle={{ justifyContent: 'space-between' }}
    />
  );
};

export default StoreCoupons;
