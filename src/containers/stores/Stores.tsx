import React, { useState } from 'react';
import { FlatList } from 'react-native';
import { useQuery } from '@apollo/react-hooks';
import StoreBox from '@components/StoreBox';
import { GET_STORES } from '@graphql/query';

const Stores = () => {

  const [stores, setStore] = useState([]);
  const [offset, setOffset] = useState(0);

  const onLoadComplete = (data: any) => {
    if (data?.getStores) {
      setStore([...stores, ...data?.getStores])
    }
  }

  const { refetch } = useQuery(GET_STORES, {
    variables: {
      offset: offset,
      limit: 6,
    },
    onCompleted: onLoadComplete
  });

  const onEndReached = () => {
    setOffset(offset + 6)
    refetch()
  }
  
  return (
    <FlatList
      data={stores}
      onEndReached={onEndReached}
      onEndReachedThreshold={0.5}
      numColumns={2}
      renderItem={({ item }: { item: any }) => {
        return (
          <StoreBox
            id={item.id}
            name={item.name}
            logo={item.logo}
            amount={item.amount}
            distance={item.distance}
          />
        );
      }}
      keyExtractor={item => item.id}
      contentContainerStyle={{ padding: 20 }}
      columnWrapperStyle={{justifyContent:'space-between'}}
    />
  );
}

export default Stores;
