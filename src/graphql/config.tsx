import ApolloClient from 'apollo-boost';

export const client = new ApolloClient({
  // uri: 'http://172.20.10.2:7001/query',
  // uri: 'http://localhost:7001/query',
  uri: 'http://user.binbindev.com/query',
});
