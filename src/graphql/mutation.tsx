import gql from 'graphql-tag';

export const SET_TOKEN_MESSAGING = gql`
  mutation SetTokenMessaging($input: TokenMessagingInput!) {
    setTokenMessaging(input: $input) {
      success
    }
  }
`;

export const REQUEST_CREATE_USER = gql`
  mutation CreateUser($input: NewUser!) {
    createUser(input: $input) {
      success
    }
  }
`;

export const IS_PHONE_EXIST = gql`
  mutation IsPhoneExist($input: IsPhoneExistInput!) {
    isPhoneExist(input: $input) {
      success
    }
  }
`;

export const IS_EMAIL_EXIST = gql`
  mutation IsEmailExist($input: IsEmailExistInput!) {
    isEmailExist(input: $input) {
      success
    }
  }
`;

export const REQUEST_OTP = gql`
  mutation RequestOTP($input: RequestOTPInput!) {
    requestOTP(input: $input) {
      success
    }
  }
`;

export const VERIFY_OTP = gql`
  mutation VerifyOTP($input: VerifyOTPInput!) {
    verifyOTP(input: $input) {
      success
    }
  }
`;

export const COLLECT_POINT = gql`
  mutation CollectPoint($input: CollectPointInput!) {
    collectPoint(input: $input) {
      success
    }
  }
`;

export const VERIFY_PASSCODE = gql`
  mutation VerifyPasscode($input: VerifyPasscodeInput!) {
    verifyPasscode(input: $input) {
      success
    }
  }
`;

export const REDEEM_COUPON = gql`
  mutation RedeemCoupon($input: RedeemCouponInput!) {
    redeemCoupon(input: $input) {
      success
    }
  }
`;

export const TRANSFER_POINT = gql`
  mutation TransferPoint($input: TransferPointInput!) {
    transferPoint(input: $input) {
      success
    }
  }
`;

export const FAVORITE_COUPON = gql`
  mutation FavoriteCoupon($input: FavoriteCouponInput!) {
    favoriteCoupon(input: $input) {
      success
    }
  }
`;

export const UNFAVORITE_COUPON = gql`
  mutation UnFavoriteCoupon($input: UnFavoriteCouponInput!) {
    unFavoriteCoupon(input: $input) {
      success
    }
  }
`;

export const UPDATE_USER = gql`
  mutation UpdateUser($input: UpdateUserInput!) {
    updateUser(input: $input) {
      success
    }
  }
`;
