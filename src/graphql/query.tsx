import gql from 'graphql-tag';

export const GET_USER = gql`
  query {
    getUser {
      firstname
      lastname
      phone
      point
      photo
    }
  }
`;

export const GET_USER_POINT = gql`
  query {
    getUser {
      point
    }
  }
`;

export const GET_USER_BYPHONE = gql`
  query GetUserByPhone($phone: String!) {
    getUserByPhone(phone: $phone) {
      firstname
      lastname
      phone
      point
      photo
    }
  }
`;

export const GET_COUPON_CATEGORIES = gql`
  query GetCouponCategories($offset: Int!, $limit: Int!) {
    getCouponCategories(offset: $offset, limit: $limit) {
      id
      name
    }
  }
`;

export const GET_STORE_CATEGORIES = gql`
  query GetStoreCategories($offset: Int!, $limit: Int!) {
    getStoreCategories(offset: $offset, limit: $limit) {
      id
      name
    }
  }
`;

export const GET_COUPONS = gql`
  query GetCoupons($offset: Int!, $limit: Int!) {
    getCoupons(offset: $offset, limit: $limit) {
      id
      name
      photo
      point
      expire
      store {
        name
      }
    }
  }
`;

export const GET_COUPON = gql`
  query GetCoupon($couponID: String!) {
    getCoupon(couponID: $couponID) {
      id
      name
      description
      photo
      condition
      point
      expire
      isFavorite
      store {
        name
      }
    }
  }
`;

export const GET_COUPON_BY_CATEGORY = gql`
  query GetCouponsByCatgory($categoryID: String!, $offset: Int!, $limit: Int!) {
    getCouponsByCategory(
      categoryID: $categoryID
      offset: $offset
      limit: $limit
    ) {
      id
      name
      photo
      point
      expire
      store {
        name
      }
    }
  }
`;

export const GET_COUPON_BY_SEARCH = gql`
  query GetCouponsBySearch($keyword: String!, $offset: Int!, $limit: Int!) {
    getCouponsBySearch(keyword: $keyword, offset: $offset, limit: $limit) {
      id
      name
      photo
      point
      expire
      store {
        name
      }
    }
  }
`;

export const GET_STORES = gql`
  query GetStores($offset: Int!, $limit: Int!) {
    getStores(offset: $offset, limit: $limit) {
      id
      logo
      name
      tagline
    }
  }
`;

export const GET_STORE = gql`
  query GetStore($storeID: String!) {
    getStore(storeID: $storeID) {
      id
      logo
      name
      tagline
      phone
      latitude
      longitude
      category {
        name
      }
      coupons(offset: 0, limit: 100) {
        id
        name
        photo
        point
        expire
        store {
          name
        }
      }
    }
  }
`;

export const GET_STORES_BY_CATEGORY = gql`
  query GetStoresByCategory($categoryID: String!, $offset: Int!, $limit: Int!) {
    getStoresByCategory(
      categoryID: $categoryID
      offset: $offset
      limit: $limit
    ) {
      id
      logo
      name
      tagline
    }
  }
`;

export const GET_STORES_BY_SEARCH = gql`
  query GetStoresBySearch($keyword: String!, $offset: Int!, $limit: Int!) {
    getStoresBySearch(keyword: $keyword, offset: $offset, limit: $limit) {
      id
      logo
      name
      tagline
    }
  }
`;

export const GET_FAVORITE_COUPONS = gql`
  query GetFavoriteCoupons($limit: Int!, $offset: Int!) {
    getFavoriteCoupons(offset: $offset, limit: $limit) {
      id
      name
      photo
      point
      expire
      store {
        name
      }
    }
  }
`;

export const GET_FAVORITE_STORES = gql`
  query GetFavoriteCoupons {
    getFavoriteStores {
      id
      logo
      name
      latitude
      longitude
    }
  }
`;

export const GET_TRASH_BYCODE = gql`
  query GetTrashByCode($code: String!) {
    getTrashByCode(code: $code) {
      id
      name
      code
      salePoint
    }
  }
`;

export const GET_TRASHES = gql`
  query GetTrashes($offset: Int!, $limit: Int!) {
    getTrashes(offset: $offset, limit: $limit) {
      id
      name
      code
      salePoint
      regularPoint
    }
  }
`;

export const GET_BINS = gql`
  query GetBins($offset: Int!, $limit: Int!) {
    getBins(offset: $offset, limit: $limit) {
      id
      latitude
      longitude
    }
  }
`;

export const GET_TRANSACTIONS = gql`
  query GetTransactions($offset: Int!, $limit: Int!) {
    getTransactions(offset: $offset, limit: $limit) {
      id
      type
      point
      description
      createdAt
    }
  }
`;

export const GET_TRANSACTION = gql`
  query GetTransaction($transactionID: String!) {
    getTransaction(transactionID: $transactionID) {
      id
      type
      point
      description
      createdAt
    }
  }
`;
