import React from 'react';
import { View, Text } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/Feather';
import CouponStack from '@containers/screens/coupon';
import StoreStack from '@containers/screens/store';
import ScanStack from '@containers/screens/scan';
import TransactionStack from '@containers/screens/transaction';
import ProfileStack from '@containers/screens/profile';
import colors from '@utils/colors';

const Tab = createBottomTabNavigator();

const Navigator = () => {
  return (
    <Tab.Navigator
      initialRouteName="Coupon"
      tabBarOptions={{
        activeTintColor: colors.primaryColor,
        inactiveTintColor: colors.fontColor,
        style: {
          height: 100,
          paddingTop: 15,
          borderTopColor: colors.iconColor,
          alignItems: 'center',
        },
        labelStyle: {
          fontFamily: 'DBHeavent',
          fontSize: 22,
          fontWeight: 'normal',
          fontStyle: 'normal',
          letterSpacing: 0,
        },
        allowFontScaling: true,
      }}>
      <Tab.Screen
        name="Coupon"
        component={CouponStack}
        options={{
          tabBarLabel: 'คูปอง',
          tabBarIcon: ({ color, size }) => (
            <Icon name="home" size={22} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="Store"
        component={StoreStack}
        options={{
          tabBarLabel: 'ร้านค้า',
          tabBarIcon: ({ color, size }) => (
            <Icon name="shopping-bag" size={22} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="Scan"
        component={ScanStack}
        options={{
          tabBarLabel: () => null,
          tabBarIcon: ({ color, size }) => {
            return (
              <View
                style={{
                  backgroundColor: colors.primaryColor,
                  borderRadius: 5,
                  paddingTop: 10,
                  paddingBottom: 5,
                  paddingHorizontal: 15,
                  alignItems: 'center',
                }}>
                <Icon name="grid" size={22} color="#ffffff" />
                <Text
                  style={{
                    fontFamily: 'DBHeavent',
                    fontSize: 22,
                    fontWeight: 'normal',
                    fontStyle: 'normal',
                    letterSpacing: 0,
                    color: '#ffffff',
                  }}>
                  รับแต้ม
                </Text>
              </View>
            );
          },
        }}
      />
      <Tab.Screen
        name="Transaction"
        component={TransactionStack}
        options={{
          tabBarLabel: 'รายการ',
          tabBarIcon: ({ color, size }) => (
            <Icon name="list" size={22} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileStack}
        options={{
          tabBarLabel: 'โปรไฟล์',
          tabBarIcon: ({ color, size }) => (
            <Icon name="user" size={22} color={color} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default Navigator;
