import { USER_LOGIN, USER_LOGOUT } from '../actions/auth';

const initialState = {
  accessToken: '',
};

function reducer(state: Object = initialState, action: any) {
  switch (action.type) {
    case USER_LOGIN:
      return { ...state, ...action.data };
    case USER_LOGOUT:
      return { ...initialState };
    default:
      return state;
  }
}

export default reducer;
