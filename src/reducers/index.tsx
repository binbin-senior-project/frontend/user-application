import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import registerReducer from './register';
import authReducer from './auth';
import userReducer from './user';
import searchReducer from './search';

const reducers = combineReducers({
  register: registerReducer,
  auth: authReducer,
  user: userReducer,
  search: searchReducer,
});

export default createStore(reducers, applyMiddleware(thunk));
