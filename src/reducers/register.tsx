import {
  REGISTER_RESET,
  REGISTER_SUCCESS,
  REGISTER_SET_PHONE,
  REGISTER_SET_EMAIL,
  REGISTER_SET_PASSWORD,
  REGISTER_SET_PASSCODE,
  REGISTER_SET_FIRSTNAME,
  REGISTER_SET_LASTNAME,
} from '../actions/register';

const initialState = {
  firstname: '',
  lastname: '',
  phone: '',
  email: '',
  password: '',
  passcode: '',
};

function reducer(state: Object = initialState, action: any) {
  switch (action.type) {
    case REGISTER_RESET:
      return { ...initialState };
    case REGISTER_SET_PHONE:
      return { ...state, phone: action.phone };
    case REGISTER_SET_FIRSTNAME:
      return { ...state, firstname: action.firstname };
    case REGISTER_SET_LASTNAME:
      return { ...state, lastname: action.lastname };
    case REGISTER_SET_EMAIL:
      return { ...state, email: action.email };
    case REGISTER_SET_PASSWORD:
      return { ...state, password: action.password };
    case REGISTER_SET_PASSCODE:
      return { ...state, passcode: action.passcode };
    case REGISTER_SUCCESS:
      return { ...initialState };
    default:
      return state;
  }
}

export default reducer;
