import {
  SET_COUPON_SEARCH,
  SET_STORE_SEARCH,
  RESET_SEARCH,
} from '@actions/search';

const initialState = {
  coupon: '',
  store: '',
  map: '',
};

function searchReducer(state: Object = initialState, action: any) {
  switch (action.type) {
    case SET_COUPON_SEARCH:
      return { ...state, coupon: action.keyword };
    case SET_STORE_SEARCH:
      return { ...state, store: action.keyword };
    case RESET_SEARCH:
      return { ...initialState };
    default:
      return state;
  }
}

export default searchReducer;
