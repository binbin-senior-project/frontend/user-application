import { UPDATE_PROFILE, UPDATE_LATEST_POST } from '../actions/user';

const initialState = {
  firstname: '',
  lastname: '',
  phone: '',
  photo: '',
  point: 0,
};

function reducer(state: Object = initialState, action: any) {
  switch (action.type) {
    case UPDATE_PROFILE:
      return { ...state, ...action.data };
    case UPDATE_LATEST_POST:
      return { ...state, point: action.point };
    default:
      return state;
  }
}

export default reducer;
