export default {
  primaryColor: '#3e9d1a',
  secondaryColor: '#2e8a0b',
  thirdaryColor: '#ffc410',
  fourthColor: '#ffffff',
  fontColor: '#222222',
  iconColor: '#d1d1d1',
  inputBackgroundColor: '#eaeaea',
  pageBackgroundColor: '#f0f0f0',
  whiteBorder: '#f2f2f2',
  grayBorder: '#f1f1f1',
  redColor: '#ff003e',
  whiteColor: '#ffffff',
};
