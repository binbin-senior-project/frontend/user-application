import logo from '@images/PromotionShopIcon/dunkin-donut-icon.png';

export const user = {
  id: 'UID',
  email: 'imkopkap@gmail.com',
  firstname: 'Sarayut',
  lastname: 'Lawilai',
  point: '10.20',
  image: '',
};

export const activities = [
  {
    id: 1,
    image: 'https://i.imgur.com/XezP5UU.png',
  },
  {
    id: 2,
    image: 'https://i.imgur.com/LzmmRVT.png',
  },
];

export const coupons = [
  {
    id: 1,
    logo,
    photo: 'https://i.imgur.com/U8bqoaG.jpg',
    name: 'แลกรับโดนัท 1 ชิ้นฟรีเมื่อซื้อกาแฟพร้อมโอวัลติน',
    store: 'Dunkin Donut Thailand',
    point: 10,
    expire: '12 ก.ย. 2562',
    distance: 1.4,
  },
  {
    id: 2,
    logo,
    photo: 'https://i.imgur.com/McpsT3P.png',
    name: 'แลกรับโดนัท 1 ชิ้นฟรีเมื่อซื้อกาแฟพร้อมโอวัลติน',
    store: 'Dunkin Donut Thailand',
    point: 10,
    expire: '12 ก.ย. 2562',
    distance: 1.4,
  },
  {
    id: 3,
    logo,
    photo: 'https://i.imgur.com/cJYjvKP.png',
    name: 'แลกรับโดนัท 1 ชิ้นฟรีเมื่อซื้อกาแฟพร้อมโอวัลติน',
    store: 'Dunkin Donut Thailand',
    point: 10,
    expire: '12 ก.ย. 2562',
    distance: 1.4,
  },
];

export const stores = [
  {
    id: 1,
    name: 'Dunkin Donut Thailand',
    description: 'พบกับโปรโมชั่นสุดคุ้มได้ที่ร้านดันกิ้นโดนัท ใกล้บ้านคุณ',
    amount: 10,
    logo,
    distance: 10.2,
  },
  {
    id: 1,
    name: 'Dunkin Donut Thailand',
    description: 'พบกับโปรโมชั่นสุดคุ้มได้ที่ร้านดันกิ้นโดนัท ใกล้บ้านคุณ',
    amount: 10,
    logo,
    distance: 10.2,
  },
  {
    id: 1,
    name: 'Dunkin Donut Thailand',
    description: 'พบกับโปรโมชั่นสุดคุ้มได้ที่ร้านดันกิ้นโดนัท ใกล้บ้านคุณ',
    amount: 10,
    logo,
    distance: 10.2,
  },
  {
    id: 1,
    name: 'Dunkin Donut Thailand',
    description: 'พบกับโปรโมชั่นสุดคุ้มได้ที่ร้านดันกิ้นโดนัท ใกล้บ้านคุณ',
    amount: 10,
    logo,
    distance: 10.2,
  },
];

export const store = {
  logo,
  name: 'Dunkin Donut Thailand',
  tagline:
    'พบกับโปรโมชั่นสุดคุ้มได้ที่ร้านดังกิ้นโดนัทใกล้บ้านคุณ เพิ่มสีสันความอร่อยในโอกาสพิเศษ ต่างๆ ด้วยดังกิ้นโดนัท',
  address: 'อ.พุทธมณฑล ต.ศาลายา จ.นครปฐม 73170',
  latitude: 37.78825,
  longitude: -122.4324,
  type: 'ร้านอาหาร',
  images: [
    'https://cdn.pixabay.com/photo/2016/03/02/20/13/shopping-1232944_1280.jpg',
    'https://cdn.pixabay.com/photo/2016/11/29/10/09/bakery-1868925_1280.jpg',
  ],
  coupons: coupons,
};

export const coupon = {
  id: 1,
  logo,
  photo: 'https://i.imgur.com/U8bqoaG.jpg',
  name: 'แลกรับโดนัท 1 ชิ้นฟรีเมื่อซื้อกาแฟพร้อมโอวัลติน',
  store: 'Dunkin Donut Thailand',
  point: 10,
  expire: '12 ก.ย. 2562',
  distance: 1.4,
};

export const tabs = [
  {
    id: 0,
    title: 'ทั้งหมด',
  },
  {
    id: 1,
    title: 'อาหาร',
  },
  {
    id: 2,
    title: 'ท่องเที่ยว',
  },
];
